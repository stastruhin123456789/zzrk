package com.project.zzrk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZzrkApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZzrkApplication.class, args);
	}

}
