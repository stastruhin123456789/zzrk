package com.project.zzrk.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class TestController {
    @GetMapping(value="/test")
    public String showTestPage1(
            @RequestParam(value = "step", defaultValue = "1") int step
    ){
        if (step == 1) {
            System.out.println(step);
            return "forward:/test?step=2";
        }
        if (step == 2) {
            System.out.println(step);
            return "test";
            
        }
        return "test";
    }
}
