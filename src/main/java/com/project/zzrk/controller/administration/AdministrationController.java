package com.project.zzrk.controller.administration;

import com.project.zzrk.service.BoxService;
import com.project.zzrk.service.ShiftService;
import com.project.zzrk.service.SubdivisionService;
import com.project.zzrk.service.UnitService;
import com.project.zzrk.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/administration")
public class AdministrationController {

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private UnitService unitService;

    @Autowired
    private ShiftService shiftService;

    @Autowired
    private BoxService boxService;

    @Autowired
    private SubdivisionService subdivisionService;

    @RequestMapping("")
    public String administration(Model model) {
        model.addAttribute("users", userService.findAll());
        model.addAttribute("units", unitService.findAll());
        model.addAttribute("shifts", shiftService.findAll());
        model.addAttribute("boxes", boxService.findAll());
        model.addAttribute("subdivisions", subdivisionService.findAll());
        return "administration/administration";
    }
}
