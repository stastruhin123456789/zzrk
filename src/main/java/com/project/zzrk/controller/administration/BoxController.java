package com.project.zzrk.controller.administration;

import com.project.zzrk.model.Box;
import com.project.zzrk.service.BoxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/administration")
public class BoxController {
    @Autowired
    private BoxService service;
    @RequestMapping("/boxes")
    public String index(Model model) {
        model.addAttribute("existsAll", service.existsAll());
        model.addAttribute("boxes", service.findAll());
        return "administration/box/index";
    }
    @GetMapping("/box/create")
    public String boxCreate(Model model){
        model.addAttribute("box", new Box());
        return "administration/box/create";
    }
    @PostMapping("/box/create")
    public String boxCreatePost(
            @ModelAttribute Box boxPost, Model model,
            RedirectAttributes redirectAttributes
    ){
        var responseStatus = service.create(boxPost);
        model.addAttribute("responseStatus", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("box", boxPost);
            return "administration/box/create";
        }
        redirectAttributes.addFlashAttribute("responseStatus", responseStatus);
        return "redirect:/administration/boxes";
    }
    @GetMapping("/box/{id}/edit")
    public String boxEdit(@PathVariable(value = "id") int id, Model model) {
        if (!service.existsById(id)) {
            return "redirect:/administration/boxes";
        }
        model.addAttribute("box", service.findById(id));
        return "administration/box/edit";
    }
    @PostMapping("/box/{id}/edit")
    public String boxEditPost(
            @PathVariable(value = "id") int id,
            @ModelAttribute Box boxPost, Model model,
            RedirectAttributes redirectAttributes
    ) {
        var responseStatus = service.update(boxPost);
        model.addAttribute("responseStatus", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("box", boxPost);
            return "administration/box/edit";
        }
        redirectAttributes.addFlashAttribute("responseStatus", responseStatus);
        return "redirect:/administration/boxes";
    }
}
