package com.project.zzrk.controller.administration;

import com.project.zzrk.dto.PlanDto;
import com.project.zzrk.model.Plan;
import com.project.zzrk.service.PlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.YearMonth;

@Controller
@RequestMapping("/administration")
public class PlanController {
    @Autowired
    private PlanService service;

    @RequestMapping("/plans")
    public String index(Model model) {
        model.addAttribute("existsAll", service.existsAll());
        model.addAttribute("existsUnitAll", service.existsUnitAll());
        model.addAttribute("existsSubdivisionAll", service.existsSubdivisionAll());
        model.addAttribute("existsUnitReportSubdivisionAll", service.existsUnitReportSubdivisionAll());

        model.addAttribute("plans", service.findAll());
        return "administration/plan/index";
    }

    @GetMapping("/plan/create")
    public String planCreate1(
            @RequestParam(value = "step", defaultValue = "1") int step,
            @RequestParam(value = "subdivisionId", defaultValue = "0") int subdivisionId,
            @RequestParam(value = "dateYearMonth", defaultValue = "") YearMonth dateYearMonth,
            Model model) {
        if(!service.existsUnitAll() || !service.existsSubdivisionAll() || !service.existsUnitReportSubdivisionAll())
            return "redirect:/administration/plans";
        if (step != 1 && step != 2 || step == 2 && dateYearMonth == null)
            return "redirect:/administration/plans";

        PlanDto planDto = null;
        if (step == 1) {
            planDto = new PlanDto(YearMonth.now(), subdivisionId);
        }
        if (step == 2){
            planDto = new PlanDto(dateYearMonth, subdivisionId);
            planDto.setListPlan(
                    service.findByDateYearMonthAndSubdivisionId(planDto)
            );
        }
        model.addAttribute("subdivisions", service.findSubdivisionAll());
        model.addAttribute("planDto", planDto);
        return "administration/plan/create";
    }

    @PostMapping("/plan/create")
    public String planCreatePost(
            @RequestParam(value = "step", defaultValue = "1") int step,
            @ModelAttribute PlanDto planDtoPost,
            Model model, RedirectAttributes redirectAttributes
    ) {
        if (step != 1 && step != 2)
            return "redirect:/administration/plans";

        var responseStatus = service.create(planDtoPost, step);
        model.addAttribute("rs", responseStatus);
        if (responseStatus.isError()) {
            model.addAttribute("subdivisions", service.findSubdivisionAll());
            model.addAttribute("planDto", planDtoPost);
            return "administration/plan/create";
        }

        if (step == 1) {
            if (planDtoPost.getDayValueForMonth() != 0)
                return "redirect:/administration/plans";
            redirectAttributes.addAttribute("dateYearMonth", planDtoPost.getDateYearMonth());
            redirectAttributes.addAttribute("subdivisionId", planDtoPost.getSubdivisionId());
            return "redirect:/administration/plan/create?step=2";
        }
        redirectAttributes.addFlashAttribute("rs", responseStatus);
        return "redirect:/administration/plans";
    }

    @GetMapping("/plan/{id}/edit")
    public String planEdit(@PathVariable(value = "id") int id, Model model) {
        if (!service.existsById(id)) {
            return "redirect:/administration/plans";
        }
        model.addAttribute("plan", service.findById(id));
        return "administration/plan/edit";
    }

    @PostMapping("/plan/{id}/edit")
    public String planEditPost(
            @PathVariable(value = "id") int id,
            @ModelAttribute Plan planPost, Model model,
            RedirectAttributes redirectAttributes
    ) {
        if (!service.existsById(id)) {
            return "redirect:/administration/plans";
        }
        var responseStatus = service.update(planPost);
        model.addAttribute("rs", responseStatus);
        if (responseStatus.isError()) {
            model.addAttribute("plan", planPost);
            return "administration/plan/edit";
        }
        redirectAttributes.addFlashAttribute("rs", responseStatus);
        return "redirect:/administration/plans";
    }

}
