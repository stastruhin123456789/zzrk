package com.project.zzrk.controller.administration;

import com.project.zzrk.model.Shift;
import com.project.zzrk.service.ShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/administration")
public class ShiftController {
    @Autowired
    private ShiftService service;
    @RequestMapping("/shifts")
    public String index(Model model) {
        model.addAttribute("existsAll", service.existsAll());
        model.addAttribute("shifts", service.findAll());
        return "administration/shift/index";
    }
    @GetMapping("/shift/create")
    public String shiftCreate(Model model){
        model.addAttribute("shift", new Shift());
        return "administration/shift/create";
    }
    @PostMapping("/shift/create")
    public String shiftCreatePost(
            @ModelAttribute Shift shiftPost, Model model,
            RedirectAttributes redirectAttributes
    ){
        var responseStatus = service.create(shiftPost);
        model.addAttribute("responseStatus", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("shift", shiftPost);
            return "administration/shift/create";
        }
        redirectAttributes.addFlashAttribute("responseStatus", responseStatus);
        return "redirect:/administration/shifts";
    }
    @GetMapping("/shift/{id}/edit")
    public String shiftEdit(@PathVariable(value = "id") int id, Model model) {
        if (!service.existsById(id)) {
            return "redirect:/administration/shifts";
        }
        model.addAttribute("shift", service.findById(id));
        return "administration/shift/edit";
    }
    @PostMapping("/shift/{id}/edit")
    public String shiftEditPost(
            @PathVariable(value = "id") int id,
            @ModelAttribute Shift shiftPost, Model model,
            RedirectAttributes redirectAttributes
    ) {
        var responseStatus = service.update(shiftPost);
        model.addAttribute("responseStatus", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("shift", shiftPost);
            return "administration/shift/edit";
        }
        redirectAttributes.addFlashAttribute("responseStatus", responseStatus);
        return "redirect:/administration/shifts";
    }
}
