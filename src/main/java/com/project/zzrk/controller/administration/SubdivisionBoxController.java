package com.project.zzrk.controller.administration;

import com.project.zzrk.model.SubdivisionBox;
import com.project.zzrk.service.SubdivisionBoxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/administration")
public class SubdivisionBoxController {
    @Autowired
    private SubdivisionBoxService service;

    @RequestMapping("/subdivision-boxes")
    public String index(Model model){
        model.addAttribute("existsAll", service.existsAll());
        model.addAttribute("existsSubdivisionAll", service.existsSubdivisionAll());
        model.addAttribute("existsBoxAll", service.existsBoxAll());

        model.addAttribute("subdivisions", service.findSubdivisionAll());
        return "administration/subdivisionbox/index";
    }

    @GetMapping("/subdivision-box/create")
    public String createSubdivisionBox(Model model){
        model.addAttribute("subdivisionBox", new SubdivisionBox());
        model.addAttribute("subdivisions", service.findSubdivisionAll());
        model.addAttribute("boxes", service.findBoxAll());
        return "administration/subdivisionbox/create";
    }

    @PostMapping("/subdivision-box/create")
    public String createPostSubdivisionBox(
            @ModelAttribute SubdivisionBox subdivisionBoxPost, Model model,
            RedirectAttributes redirectAttributes
    ){
        var responseStatus = service.create(subdivisionBoxPost);
        model.addAttribute("rs", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("subdivisionBox", subdivisionBoxPost);
            model.addAttribute("subdivisions", service.findSubdivisionAll());
            model.addAttribute("boxes", service.findBoxAll());
            return "administration/subdivisionbox/create";
        }
        redirectAttributes.addFlashAttribute("rs", responseStatus);
        return "redirect:/administration/subdivision-boxes";
    }
    @GetMapping("/subdivision-box/{id}/edit")
    public String editSubdivisionBox(
            @PathVariable(value = "id") int id,
            Model model
    ){
        if (!service.existsById(id)) {
            return "redirect:/administration/subdivision-boxes";
        }
        model.addAttribute("subdivisionBox", service.findById(id));
        model.addAttribute("subdivisions", service.findSubdivisionAll());
        model.addAttribute("boxes", service.findBoxAll());
        return "administration/subdivisionbox/edit";
    }

    @PostMapping("/subdivision-box/{id}/edit")
    public String editPostSubdivisionBox(
            @PathVariable(value = "id") int id,
            @ModelAttribute SubdivisionBox subdivisionBoxPost,
            Model model,
            RedirectAttributes redirectAttributes
    ){
        if (!service.existsById(id)) {
            return "redirect:/administration/subdivision-boxes";
        }
        var responseStatus = service.update(subdivisionBoxPost);
        model.addAttribute("rs", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("subdivisionBox", subdivisionBoxPost);
            model.addAttribute("subdivisions", service.findSubdivisionAll());
            model.addAttribute("boxes", service.findBoxAll());
            return "administration/subdivisionbox/edit";
        }
        redirectAttributes.addFlashAttribute("rs", responseStatus);
        return "redirect:/administration/subdivision-boxes";
    }
}
