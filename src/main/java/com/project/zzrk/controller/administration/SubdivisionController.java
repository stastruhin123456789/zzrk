package com.project.zzrk.controller.administration;

import com.project.zzrk.model.Subdivision;
import com.project.zzrk.service.SubdivisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/administration")
public class SubdivisionController {
    @Autowired
    private SubdivisionService service;
    @RequestMapping("/subdivisions")
    public String index(Model model) {
        model.addAttribute("existsAll", service.existsAll());
        model.addAttribute("subdivisions", service.findAll());
        return "administration/subdivision/index";
    }
    @GetMapping("/subdivision/create")
    public String subdivisionCreate(Model model){
        model.addAttribute("subdivision", new Subdivision());
        return "administration/subdivision/create";
    }
    @PostMapping("/subdivision/create")
    public String subdivisionCreatePost(
            @ModelAttribute Subdivision subdivisionPost, Model model,
            RedirectAttributes redirectAttributes
    ){
        var responseStatus = service.create(subdivisionPost);
        model.addAttribute("responseStatus", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("subdivision", subdivisionPost);
            return "administration/subdivision/create";
        }
        redirectAttributes.addFlashAttribute("responseStatus", responseStatus);
        return "redirect:/administration/subdivisions";
    }
    @GetMapping("/subdivision/{id}/edit")
    public String subdivisionEdit(@PathVariable(value = "id") int id, Model model) {
        if (!service.existsById(id)) {
            return "redirect:/administration/subdivisions";
        }
        model.addAttribute("subdivision", service.findById(id));
        return "administration/subdivision/edit";
    }
    @PostMapping("/subdivision/{id}/edit")
    public String subdivisionEditPost(
            @PathVariable(value = "id") int id,
            @ModelAttribute Subdivision subdivisionPost, Model model,
            RedirectAttributes redirectAttributes
    ) {
        var responseStatus = service.update(subdivisionPost);
        model.addAttribute("responseStatus", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("subdivision", subdivisionPost);
            return "administration/subdivision/edit";
        }
        redirectAttributes.addFlashAttribute("responseStatus", responseStatus);
        return "redirect:/administration/subdivisions";
    }
}
