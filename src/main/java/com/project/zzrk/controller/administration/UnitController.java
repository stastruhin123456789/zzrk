package com.project.zzrk.controller.administration;

import com.project.zzrk.model.Unit;
import com.project.zzrk.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/administration")
public class UnitController {
    @Autowired
    private UnitService service;
    @RequestMapping("/units")
    public String index(Model model) {
        model.addAttribute("existsAll", service.existsAll());
        model.addAttribute("units", service.findAll());
        return "administration/unit/index";
    }
    @GetMapping("/unit/create")
    public String unitCreate(Model model){
        model.addAttribute("unit", new Unit());
        return "administration/unit/create";
    }
    @PostMapping("/unit/create")
    public String unitCreatePost(
            @ModelAttribute Unit unitPost, Model model,
            RedirectAttributes redirectAttributes
    ){
        var responseStatus = service.create(unitPost);
        model.addAttribute("responseStatus", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("unit", unitPost);
            return "administration/unit/create";
        }
        redirectAttributes.addFlashAttribute("responseStatus", responseStatus);
        return "redirect:/administration/units";
    }
    @GetMapping("/unit/{id}/edit")
    public String unitEdit(@PathVariable(value = "id") int id, Model model) {
        if (!service.existsById(id)) {
            return "redirect:/administration/units";
        }
        model.addAttribute("unit", service.findById(id));
        return "administration/unit/edit";
    }
    @PostMapping("/unit/{id}/edit")
    public String unitEditPost(
            @PathVariable(value = "id") int id,
            @ModelAttribute Unit unitPost, Model model,
            RedirectAttributes redirectAttributes
    ) {
        var responseStatus = service.update(unitPost);
        model.addAttribute("responseStatus", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("unit", unitPost);
            return "administration/unit/edit";
        }
        redirectAttributes.addFlashAttribute("responseStatus", responseStatus);
        return "redirect:/administration/units";
    }
}
