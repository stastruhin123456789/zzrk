package com.project.zzrk.controller.administration;

import com.project.zzrk.model.UnitConvert;
import com.project.zzrk.service.UnitConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;

@Controller
@RequestMapping("/administration")
public class UnitConvertController {
    @Autowired
    private UnitConvertService service;
    @RequestMapping("/unit-converts")
    public String index(Model model) {
        model.addAttribute("existsAll", service.existsAll());
        model.addAttribute("unitConverts", service.findAll());
        model.addAttribute("existsUnitAll", service.existsUnitAll());
        return "administration/unitconvert/index";
    }
    @GetMapping("/unit-convert/create")
    public String unitConvertCreate(Model model){
        model.addAttribute("unitConvert", new UnitConvert(LocalDate.now()));
        model.addAttribute("units", service.findUnitAll());
        return "administration/unitconvert/create";
    }
    @PostMapping("/unit-convert/create")
    public String unitConvertCreatePost(
            @ModelAttribute UnitConvert unitConvertPost, Model model,
            RedirectAttributes redirectAttributes
    ){
        var responseStatus = service.create(unitConvertPost);
        model.addAttribute("rs", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("unitConvert", unitConvertPost);
            model.addAttribute("units", service.findUnitAll());
            return "administration/unitconvert/create";
        }
        redirectAttributes.addFlashAttribute("rs", responseStatus);
        return "redirect:/administration/unit-converts";
    }
    @GetMapping("/unit-convert/{id}/edit")
    public String unitConvertEdit(@PathVariable(value = "id") int id, Model model){
        if (!service.existsById(id)) {
            return "redirect:/administration/unit-converts";
        }
        model.addAttribute("unitConvert", service.findById(id));
        model.addAttribute("units", service.findUnitAll());
        return "administration/unitconvert/edit";
    }
    @PostMapping("/unit-convert/{id}/edit")
    public String unitConvertEditPost(
            @PathVariable(value = "id") int id,
            @ModelAttribute UnitConvert unitConvertPost, Model model,
            RedirectAttributes redirectAttributes
    ) {
        if (!service.existsById(id)) {
            return "redirect:/administration/unit-converts";
        }
        var responseStatus = service.update(unitConvertPost);
        model.addAttribute("rs", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("unitConvert", unitConvertPost);
            model.addAttribute("units", service.findUnitAll());
            return "administration/unitconvert/edit";
        }
        redirectAttributes.addFlashAttribute("rs", responseStatus);
        return "redirect:/administration/unit-converts";
    }
}
