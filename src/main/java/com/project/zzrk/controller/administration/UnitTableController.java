package com.project.zzrk.controller.administration;

import com.project.zzrk.model.unittable.UnitReportBox;
import com.project.zzrk.model.unittable.UnitReportSubdivision;
import com.project.zzrk.service.unittable.UnitTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;

@Controller
@RequestMapping("/administration")
public class UnitTableController {
    @Autowired
    private UnitTableService service;

    @RequestMapping("/unit-tables")
    public String index(Model model){
        model.addAttribute("existsUnitAll", service.existsUnitAll());

        model.addAttribute("existsUnitReportSubdivisionAll", service.existsUnitReportSubdivisionAll());
        model.addAttribute("unitReportSubdivisions", service.findUnitReportSubdivisionAll());

        model.addAttribute("existsUnitReportBoxAll", service.existsUnitReportBoxAll());
        model.addAttribute("unitReportBoxes", service.findUnitReportBoxAll());

        return "administration/unittable/index";
    }

//    @GetMapping("/unit-table/{table}/create")
//    public String createUnitTable(@PathVariable("table") String table, Model model){
//        model.addAttribute("units", service.findUnitAll());
//        switch (table){
//            case "report-subdivision": {
//                model.addAttribute("unitReportSubdivision", new UnitReportSubdivision(LocalDate.now()));
//                return "administration/unittable/unitreportsubdivision/create";
//            }
//            case "report-box": {
//                model.addAttribute("unitReportBox", new UnitReportBox(LocalDate.now()));
//                return "administration/unittable/unitreportbox/create";
//            }
//            default:
//                return "redirect:administration/unit-tables";
//        }
//
//    }

    @GetMapping("/unit-table/report-subdivision/create")
    public String createUnitReportSubdivision(Model model){
        model.addAttribute("units", service.findUnitAll());
        model.addAttribute("unitReportSubdivision", new UnitReportSubdivision(LocalDate.now()));
        return "administration/unittable/unitreportsubdivision/create";
    }

    @PostMapping("/unit-table/report-subdivision/create")
    public String createPostUnitReportSubdivision(
            @ModelAttribute UnitReportSubdivision unitReportSubdivisionPost,
            Model model, RedirectAttributes redirectAttributes
    ){
        var responseStatus = service.create(unitReportSubdivisionPost);
        model.addAttribute("rs", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("units", service.findUnitAll());
            model.addAttribute("unitReportSubdivision", unitReportSubdivisionPost);
            return "administration/unittable/unitreportsubdivision/create";
        }
        redirectAttributes.addFlashAttribute("rs", responseStatus);
        return "redirect:/administration/unit-tables";
    }

    @GetMapping("/unit-table/report-subdivision/{id}/edit")
    public String editUnitReportSubdivision(
            @PathVariable("id") int id,
            Model model
    ){
        if (!service.existsUnitReportSubdivisionById(id)) {
            return "redirect:/administration/unit-tables";
        }
        model.addAttribute("units", service.findUnitAll());
        model.addAttribute("unitReportSubdivision", service.findUnitReportSubdivisionById(id));
        return "administration/unittable/unitreportsubdivision/edit";
    }

    @PostMapping("/unit-table/report-subdivision/{id}/edit")
    public String editPostUnitReportSubdivision(
            @ModelAttribute UnitReportSubdivision unitReportSubdivisionPost,
            @PathVariable("id") int id,
            Model model, RedirectAttributes redirectAttributes
    ){
        if (!service.existsUnitReportSubdivisionById(id)) {
            return "redirect:/administration/unit-tables";
        }
        var responseStatus = service.update(unitReportSubdivisionPost);
        model.addAttribute("rs", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("units", service.findUnitAll());
            model.addAttribute("unitReportSubdivision", unitReportSubdivisionPost);
            return "administration/unittable/unitreportsubdivision/edit";
        }
        redirectAttributes.addFlashAttribute("rs", responseStatus);
        return "redirect:/administration/unit-tables";
    }


    @GetMapping("/unit-table/report-box/create")
    public String createUnitBox(Model model){
        model.addAttribute("units", service.findUnitAll());
        model.addAttribute("unitReportBox", new UnitReportBox(LocalDate.now()));
        return "administration/unittable/unitreportbox/create";

    }

    @PostMapping("/unit-table/report-box/create")
    public String createPostUnitReportBox(
            @ModelAttribute UnitReportBox unitReportBoxPost,
            Model model, RedirectAttributes redirectAttributes
    ){
        var responseStatus = service.create(unitReportBoxPost);
        model.addAttribute("rs", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("units", service.findUnitAll());
            model.addAttribute("unitReportBox", unitReportBoxPost);
            return "administration/unittable/unitreportbox/create";
        }
        redirectAttributes.addFlashAttribute("rs", responseStatus);
        return "redirect:/administration/unit-tables";
    }

    @GetMapping("/unit-table/report-box/{id}/edit")
    public String editUnitReportBox(
            @PathVariable("id") int id,
            Model model
    ){
        if (!service.existsUnitReportBoxById(id)) {
            return "redirect:/administration/unit-tables";
        }
        model.addAttribute("units", service.findUnitAll());
        model.addAttribute("unitReportBox", service.findUnitReportBoxById(id));
        return "administration/unittable/unitreportbox/edit";
    }

    @PostMapping("/unit-table/report-box/{id}/edit")
    public String editPostUnitReportBox(
            @ModelAttribute UnitReportBox unitReportBoxPost,
            @PathVariable("id") int id,
            Model model, RedirectAttributes redirectAttributes
    ){
        if (!service.existsUnitReportBoxById(id)) {
            return "redirect:/administration/unit-tables";
        }
        var responseStatus = service.update(unitReportBoxPost);
        model.addAttribute("rs", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("units", service.findUnitAll());
            model.addAttribute("unitReportBox", unitReportBoxPost);
            return "administration/unittable/unitreportbox/edit";
        }
        redirectAttributes.addFlashAttribute("rs", responseStatus);
        return "redirect:/administration/unit-tables";
    }
}
