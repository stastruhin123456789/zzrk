package com.project.zzrk.controller.administration;

import com.project.zzrk.model.User;
import com.project.zzrk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/administration")
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping("/users")
    public String index(Model model) {
        model.addAttribute("existsAll", userService.existsAll());
        model.addAttribute("users", userService.findAll());
        return "administration/user/index";
    }
    @GetMapping("/user/create")
    public String userCreate(Model model){
        model.addAttribute("user", new User());
        return "administration/user/create";
    }
    @PostMapping("/user/create")
    public String userCreatePost(
            @ModelAttribute User userPost, Model model,
            RedirectAttributes redirectAttributes
    ){
        var responseStatus = userService.create(userPost);
        model.addAttribute("responseStatus", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("user", userPost);
            return "administration/user/create";
        }
        redirectAttributes.addFlashAttribute("responseStatus", responseStatus);
        return "redirect:/administration/users";
    }
    @GetMapping("/user/{id}/edit")
    public String userEdit(@PathVariable(value = "id") int id, Model model) {
        if (!userService.existsById(id)) {
            return "redirect:/administration/user";
        }
        model.addAttribute("user", userService.findById(id));
        return "administration/user/edit";
    }
    @PostMapping("/user/{id}/edit")
    public String userEditPost(
            @PathVariable(value = "id") int id,
            @ModelAttribute User userPost, Model model,
            RedirectAttributes redirectAttributes
    ) {
        var responseStatus = userService.update(userPost);
        model.addAttribute("responseStatus", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("user", userPost);
            return "administration/user/edit";
        }
        redirectAttributes.addFlashAttribute("responseStatus", responseStatus);
        return "redirect:/administration/users";
    }
}
