package com.project.zzrk.controller.report;

import com.project.zzrk.model.report.ReportBox;
import com.project.zzrk.service.report.ReportBoxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;

@Controller
@RequestMapping("/report")
public class ReportBoxController {

    @Autowired
    private ReportBoxService service;

    @RequestMapping("/boxes")
    public String reportBoxes(
            @RequestParam(value = "reportId", defaultValue = "0") int reportId,
            @RequestParam(value = "reportShiftId", defaultValue = "0") int reportShiftId,
            Model model) {
        if (!service.existsReportByReportId(reportId)) {
            return "redirect:/reports";
        }
        if (!service.existsReportShiftByReportShiftId(reportShiftId)) {
            return String.format("redirect:/report/%s", reportId);
        }
        model.addAttribute("existsAll", service.existsByReportShiftId(reportShiftId));
        LocalDate reportDate = service.findReportDateByReportId(reportId);
        model.addAttribute("existsUnitReportBoxByReportDate", service.existsUnitReportBoxByReportDate(reportDate));
        model.addAttribute("reportDate", reportDate);
        model.addAttribute("reportShift", service.findReportShiftByReportShiftId(reportShiftId));
        model.addAttribute("reportBoxes", service.findByReportShiftId(reportShiftId));
        return "report/reportbox/index";
    }

    @PostMapping("/box/create")
    public String reportBoxCreatePost(
            @RequestParam(value = "reportId", defaultValue = "0") int reportId,
            @RequestParam(value = "reportShiftId", defaultValue = "0") int reportShiftId,
            @RequestParam(value = "boxId") int boxId,
            @RequestParam(value = "boxValue") int boxValue,
            RedirectAttributes redirectAttributes
            ) {
        redirectAttributes.addFlashAttribute(
                "responseStatus", service.create(reportShiftId, boxId, boxValue)
        );
        redirectAttributes.addFlashAttribute("boxValue", boxValue);
        return String.format("redirect:/report/boxes?reportId=%s&reportShiftId=%s", reportId, reportShiftId);
    }

    @GetMapping("/box/{id}/edit")
    public String reportBoxEdit(
            @RequestParam(value = "reportId", defaultValue = "0") int reportId,
            @RequestParam(value = "reportShiftId", defaultValue = "0") int reportShiftId,
            @PathVariable("id") int id,
            Model model
    ) {
        if (!service.existsById(id)) {
            return String.format("redirect:/report/boxes?reportId=%s&reportShiftId=%s", reportId, reportShiftId);
        }
        model.addAttribute("reportShift", service.findReportShiftByReportShiftId(reportShiftId));
        model.addAttribute("reportBox", service.findById(id));
        return "report/reportbox/edit";
    }

    @PostMapping("/box/{id}/edit")
    public String reportBoxEditPost(
            @RequestParam(value = "reportId", defaultValue = "0") int reportId,
            @RequestParam(value = "reportShiftId", defaultValue = "0") int reportShiftId,
            @PathVariable("id") int id,
            @ModelAttribute ReportBox reportBoxPost,
            Model model, RedirectAttributes redirectAttributes
    ) {
        var responseStatus = service.update(reportBoxPost);
        model.addAttribute("responseStatus", responseStatus);
        if(responseStatus.isError()){
            model.addAttribute("reportBox", reportBoxPost);
            model.addAttribute("reportShift", service.findReportShiftByReportShiftId(reportShiftId));
            return "report/reportbox/edit";
        }
        redirectAttributes.addFlashAttribute("responseStatus", responseStatus);
        return String.format("redirect:/report/boxes?reportId=%s&reportShiftId=%s", reportId, reportShiftId);
    }
}
