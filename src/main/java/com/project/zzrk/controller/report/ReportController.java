package com.project.zzrk.controller.report;

import com.project.zzrk.service.report.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("")
public class ReportController {

    @Autowired
    private ReportService service;

    @RequestMapping("/reports")
    public String reports(Model model) {
        boolean existsAll = service.existsAll();
        model.addAttribute("existsAll", existsAll);
        model.addAttribute("existsSubdivisionAll", existsAll || service.existsSubdivisionAll());
        model.addAttribute("existsBoxAll", existsAll ||service.existsBoxAll());
        model.addAttribute("existsSubdivisionBoxAll", existsAll || service.existsSubdivisionBoxAll());
        model.addAttribute("existsShiftAll", existsAll || service.existsShiftAll());
        model.addAttribute("existsUnitAll", existsAll || service.existsUnitAll());
        model.addAttribute("existsUnitConvertAll", existsAll || service.existsUnitConvertAll());
        model.addAttribute("existsUnitReportSubdivisionAll", existsAll || service.existsUnitReportSubdivisionAll());
        model.addAttribute("existsPlanAll", existsAll || service.existsPlanAll());
        model.addAttribute("reports", service.findAll());
        return "report/index";
    }

    @RequestMapping("/report/create")
    public String reportCreate(RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("responseStatus", service.create());
        return "redirect:/reports";
    }

}
