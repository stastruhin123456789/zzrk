package com.project.zzrk.controller.report;

import com.project.zzrk.service.report.ReportSubdivisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/report")
public class ReportSubdivisionController {

    @Autowired
    private ReportSubdivisionService service;

    @RequestMapping("subdivisions")
    public String reportByDate(
            @RequestParam(value = "reportId", defaultValue = "0") int reportId,
            Model model
    ) {
        if (!service.existsReportByReportId(reportId)) {
            return "redirect:/reports";
        }
        model.addAttribute("reportDate", service.findReportDateByReportId(reportId));
        model.addAttribute("reportSubdivisions", service.findByReportId(reportId));
        return "report/reportsubdivision/index";
    }
}
