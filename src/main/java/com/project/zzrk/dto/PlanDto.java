package com.project.zzrk.dto;

import com.project.zzrk.model.Plan;
import com.project.zzrk.model.Subdivision;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.YearMonth;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class PlanDto {

    private YearMonth dateYearMonth;

    private int dayValueForMonth;

    private int subdivisionId;

    private List<Plan> listPlan;

    public PlanDto(YearMonth dateYearMonth, int subdivisionId) {
        this.dateYearMonth = dateYearMonth;
        this.subdivisionId = subdivisionId;
    }

    public PlanDto(YearMonth dateYearMonth, int subdivisionId, List<Plan> listPlan) {
        this.dateYearMonth = dateYearMonth;
        this.subdivisionId = subdivisionId;
        this.listPlan = listPlan;
    }
}