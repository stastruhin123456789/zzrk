package com.project.zzrk.dto.report;

import com.project.zzrk.model.Box;
import com.project.zzrk.model.report.ReportShift;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReportBoxDto {
    private int id;

    private ReportShift reportShift;

    private Box box;

    private String unitName;

    private int boxValue;
}
