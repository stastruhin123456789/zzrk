package com.project.zzrk.dto.report;

import com.project.zzrk.model.User;
import com.project.zzrk.model.report.ReportSubdivision;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReportDto {
    private int id;

    private LocalDate date;

    private LocalDateTime createdAt;

    private String userName;

    private Set<ReportSubdivision> reportSubdivisions;
}
