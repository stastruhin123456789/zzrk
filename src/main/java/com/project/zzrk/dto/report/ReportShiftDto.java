package com.project.zzrk.dto.report;

import com.project.zzrk.model.Shift;
import com.project.zzrk.model.report.ReportBox;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReportShiftDto {
    private int id;

    private Shift shift;

    private int shiftValue;

    private Set<ReportBox> reportBoxes;
}
