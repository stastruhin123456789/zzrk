package com.project.zzrk.dto.report;

import com.project.zzrk.model.report.ReportShift;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReportSubdivisionDto {
    private int id;

    private String subdivisionName;

    private String unitName;

    private int subdivisionValue;

    private Set<ReportShift> reportShifts;
}
