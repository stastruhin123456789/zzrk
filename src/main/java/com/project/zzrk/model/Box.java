package com.project.zzrk.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "boxes")
@Getter
@Setter
@NoArgsConstructor
public class Box {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "box_name")
    private String name;

    @Column(name = "is_actual")
    private boolean isActual;

//    @ManyToMany(mappedBy = "boxes", fetch = FetchType.LAZY)
//    @JsonIgnoreProperties("boxes")
//    private Set<Subdivision> subdivisions;

    @OneToMany(mappedBy = "box")
    private Set<SubdivisionBox> subdivisionBoxes;

    public boolean getIsActual() {
        return isActual;
    }

    public void setIsActual(boolean isActual) {
        this.isActual = isActual;
    }
}
