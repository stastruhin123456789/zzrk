package com.project.zzrk.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "plans")
@Getter
@Setter
@NoArgsConstructor
public class Plan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name="plan_date")
    private LocalDate date;

    @Column(name="plan_day_value")
    private int dayValue;

    @Column(name="plan_month_value")
    private int monthValue;

    @ManyToOne
    @JoinColumn(name="subdivision_id")
    private Subdivision subdivision;

    @ManyToOne
    @JoinColumn(name="unit_id")
    private Unit unit;

}
