package com.project.zzrk.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "shifts")
@Getter
@Setter
@NoArgsConstructor
public class Shift {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "shift_name")
    private String name;

    @Column(name = "is_actual")
    private boolean isActual;

    public boolean getIsActual() {
        return isActual;
    }

    public void setIsActual(boolean isActual) {
        this.isActual = isActual;
    }


}
