package com.project.zzrk.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "subdivisions")
@Getter
@Setter
@NoArgsConstructor
public class Subdivision {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "subdivision_name")
    private String name;

    @Column(name = "is_actual")
    private boolean isActual;

//    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(
//            name = "subdivision_box",
//            joinColumns = @JoinColumn(name = "subdivision_id"),
//            inverseJoinColumns = @JoinColumn(name = "box_id"))
//    @JsonIgnoreProperties("subdivisions")
//    private Set<Box> boxes;

    @OneToMany(mappedBy = "subdivision")
    private Set<SubdivisionBox> subdivisionBoxes;

    public boolean getIsActual() {
        return isActual;
    }

    public void setIsActual(boolean isActual) {
        this.isActual = isActual;
    }
}
