package com.project.zzrk.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "units")
@Getter
@Setter
@NoArgsConstructor
public class Unit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "unit_name")
    private String name;

    @Column(name = "is_actual")
    private boolean isActual;

    public boolean getIsActual() {
        return isActual;
    }

    public void setIsActual(boolean isActual) {
        this.isActual = isActual;
    }


}
