package com.project.zzrk.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "unit_converts")
@Getter
@Setter
@NoArgsConstructor
public class UnitConvert {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name="unit_convert_date")
    private LocalDate date;

    @ManyToOne
    @JoinColumn(name="from_unit_id")
    private Unit fromUnit;

    @ManyToOne
    @JoinColumn(name="to_unit_id")
    private Unit toUnit;

    @Column(name="coefficient_value")
    private int coefficient;

    @Column(name = "is_actual")
    private boolean isActual;

    public UnitConvert(LocalDate date) {
        this.date = date;
    }

    public boolean getIsActual() {
        return isActual;
    }

    public void setIsActual(boolean isActual) {
        this.isActual = isActual;
    }
}
