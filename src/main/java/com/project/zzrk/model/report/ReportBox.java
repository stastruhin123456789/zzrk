package com.project.zzrk.model.report;

import com.project.zzrk.model.Box;
import com.project.zzrk.model.Unit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "report_boxes")
@Getter
@Setter
@NoArgsConstructor
public class ReportBox {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="report_shift_id")
    private ReportShift reportShift;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="box_id")
    private Box box;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="unit_id")
    private Unit unit;

    @Column(name = "box_value")
    private int value;
}
