package com.project.zzrk.model.report;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.project.zzrk.model.Shift;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "report_shifts")
@Getter
@Setter
@NoArgsConstructor
public class ReportShift {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="report_subdivision_id")
    @JsonIgnore
    private ReportSubdivision reportSubdivision;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="shift_id")
    private Shift shift;

    @Column(name = "shift_value")
    private int value;

    @OneToMany(mappedBy = "reportShift", fetch = FetchType.LAZY)
    private Set<ReportBox> reportBoxes;
}
