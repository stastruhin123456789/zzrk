package com.project.zzrk.model.report;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.project.zzrk.model.Subdivision;
import com.project.zzrk.model.Unit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "report_subdivisions")
@Getter
@Setter
@NoArgsConstructor
public class ReportSubdivision {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="report_id")
    @JsonIgnore
    private Report report;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="subdivision_id")
    private Subdivision subdivision;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="unit_id")
    private Unit unit;

    @Column(name = "subdivision_value")
    private int subdivisionValue;

    @Column(name = "plan_day_value")
    private int planDayValue;

    @Column(name = "deviation_day_value")
    private int deviationDayValue;

    @Column(name = "subdivision_month_value")
    private int subdivisionMonthValue;

    @Column(name = "plan_month_value")
    private int planMonthValue;

    @Column(name = "deviation_month_value")
    private int deviationMonthValue;

    @OneToMany(mappedBy = "reportSubdivision", fetch = FetchType.LAZY)
    private Set<ReportShift> reportShifts;
}
