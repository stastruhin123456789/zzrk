package com.project.zzrk.model.unittable;

import com.project.zzrk.model.Unit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "unit_report_subdivisions")
@Getter
@Setter
@NoArgsConstructor
public class UnitReportSubdivision {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "unit_date")
    private LocalDate date;

    @ManyToOne
    @JoinColumn(name="unit_id")
    private Unit unit;

    @Column(name = "is_actual")
    private boolean isActual;

    public UnitReportSubdivision(LocalDate date) {
        this.date = date;
    }

    public boolean getIsActual() {
        return isActual;
    }

    public void setIsActual(boolean isActual) {
        this.isActual = isActual;
    }
}
