package com.project.zzrk.repository;

import com.project.zzrk.model.Box;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BoxRepository
        extends JpaRepository<Box, Integer> {

    @Query("SELECT CASE WHEN COUNT(b) > 0 THEN true ELSE false END FROM Box AS b")
    boolean existsAll();

    @Query("SELECT b FROM Box AS b")
    List<Box> findAll();

    boolean existsByName(String name);
}
