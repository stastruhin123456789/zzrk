package com.project.zzrk.repository;

import com.project.zzrk.model.Plan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface PlanRepository extends JpaRepository<Plan, Integer> {

    @Query("SELECT CASE WHEN COUNT(p) > 0 THEN true ELSE false END FROM Plan AS p")
    boolean existsAll();


    @Query(
            value = "SELECT CASE WHEN COUNT(p.id) > 0 THEN 'true' ELSE 'false' END FROM plans AS p " +
                    "WHERE YEAR(p.plan_date) = :year AND MONTH(p.plan_date) = :month " +
                    "AND p.subdivision_id = :subdivisionId",
            nativeQuery = true
    )
    boolean existsByDateYearMonthAndSubdivisionId(
            @Param("year") int year, @Param("month") int month,
            @Param("subdivisionId")int subdivisionId);

    @Query(
            value = "SELECT * FROM plans AS p " +
                    "WHERE YEAR(p.plan_date) = :year AND MONTH(p.plan_date) = :month " +
                    "AND p.subdivision_id = :subdivisionId",
            nativeQuery = true
    )
    List<Plan> findByDateYearMonthAndSubdivisionId(
            @Param("year") int year, @Param("month") int month,
            @Param("subdivisionId") int subdivisionId);

    @Procedure("create_plan")
    void createPlan(LocalDate date, int subdivisionId, int dayValue);

    @Procedure("update_plan_month_value")
    void updatePlanMonthValue(LocalDate date, int subdivisionId);

    @Query(
            value = "SELECT CASE WHEN COUNT(p.id) = " +
                    "(SELECT COUNT(s.id) FROM subdivisions AS s WHERE is_actual = 1) " +
                    "THEN 'true' ELSE 'false' END " +
                    "FROM plans AS p WHERE plan_date = :reportDate",
            nativeQuery = true
    )
    boolean existsByReportDate(@Param("reportDate") LocalDate reportDate);
}
