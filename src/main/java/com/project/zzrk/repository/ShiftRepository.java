package com.project.zzrk.repository;

import com.project.zzrk.model.Shift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShiftRepository
        extends JpaRepository<Shift, Integer> {

    @Query("SELECT CASE WHEN COUNT(sh) > 0 THEN true ELSE false END FROM Shift AS sh")
    boolean existsAll();

    @Query("SELECT sh FROM Shift AS sh")
    List<Shift> findAll();

    boolean existsByName(String name);
}
