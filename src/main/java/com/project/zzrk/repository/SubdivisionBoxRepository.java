package com.project.zzrk.repository;

import com.project.zzrk.model.Box;
import com.project.zzrk.model.Subdivision;
import com.project.zzrk.model.SubdivisionBox;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SubdivisionBoxRepository extends JpaRepository<SubdivisionBox, Integer> {

    @Query("SELECT CASE WHEN COUNT(sb) > 0 THEN true ELSE false END FROM SubdivisionBox AS sb")
    boolean existsAll();

    boolean existsBySubdivisionAndBox(Subdivision subdivision, Box box);

    Optional<SubdivisionBox> findBySubdivisionAndBox(Subdivision subdivision, Box box);
}
