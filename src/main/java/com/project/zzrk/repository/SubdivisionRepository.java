package com.project.zzrk.repository;

import com.project.zzrk.model.Subdivision;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubdivisionRepository
        extends JpaRepository<Subdivision, Integer> {

    @Query("SELECT CASE WHEN COUNT(sub) > 0 THEN true ELSE false END FROM Subdivision AS sub")
    boolean existsAll();

    @Query("SELECT subd FROM Subdivision AS subd ORDER BY subd.id ASC")
    List<Subdivision> findAll();

    boolean existsByName(String name);
}
