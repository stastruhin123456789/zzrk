package com.project.zzrk.repository;

import com.project.zzrk.model.Unit;
import com.project.zzrk.model.UnitConvert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface UnitConvertRepository extends JpaRepository<UnitConvert, Integer> {

    @Query("SELECT CASE WHEN COUNT(unc) > 0 THEN true ELSE false END FROM UnitConvert AS unc")
    boolean existsAll();

    boolean existsByDateAndFromUnitAndToUnit(LocalDate date, Unit fromUnit, Unit toUnit);

    Optional<UnitConvert> findByDateAndFromUnitAndToUnit(LocalDate date, Unit fromUnit, Unit toUnit);
}
