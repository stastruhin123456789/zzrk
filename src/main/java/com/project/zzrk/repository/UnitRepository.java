package com.project.zzrk.repository;

import com.project.zzrk.model.Unit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UnitRepository
        extends JpaRepository<Unit, Integer> {

    @Query("SELECT CASE WHEN COUNT(un) > 0 THEN true ELSE false END FROM Unit AS un")
    boolean existsAll();

    @Query("SELECT un FROM Unit AS un")
    List<Unit> findAll();

    boolean existsByName(String name);
}
