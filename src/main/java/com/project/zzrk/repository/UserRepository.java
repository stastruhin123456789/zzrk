package com.project.zzrk.repository;

import com.project.zzrk.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN true ELSE false END FROM User AS u")
    boolean existsAll();

    @Query("SELECT u FROM User AS u")
    List<User> findAll();

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN true ELSE false END FROM User AS u WHERE u.id = :id")
    boolean existsById(@Param("id") int id);

    @Query("SELECT u FROM User AS u WHERE u.id = :id")
    Optional<User> findById(@Param("id") int id);

    boolean existsByLogin(String login);

}
