package com.project.zzrk.repository.report;

import com.project.zzrk.model.report.ReportBox;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportBoxRepository
        extends JpaRepository<ReportBox, Integer> {

    boolean existsByReportShiftId(int reportShiftId);

    List<ReportBox> findByReportShiftId(int reportShiftId);

    @Query(
            value = "SELECT CASE WHEN COUNT(rb.id) > 0 THEN 'true' ELSE 'false' END FROM report_boxes AS rb " +
                    "WHERE rb.report_shift_id = :reportShiftId AND rb.box_id = :boxId  ",
            nativeQuery = true
    )
    boolean existsByReportShiftIdAndBoxId(@Param("reportShiftId") int reportShiftId,@Param("boxId") int boxId);
}
