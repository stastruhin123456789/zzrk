package com.project.zzrk.repository.report;

import com.project.zzrk.model.report.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {

    @Query("SELECT CASE WHEN COUNT(r) > 0 THEN true ELSE false END FROM Report AS r")
    boolean existsAll();

    Optional<Report> findFirstByOrderByDateDesc();
}
