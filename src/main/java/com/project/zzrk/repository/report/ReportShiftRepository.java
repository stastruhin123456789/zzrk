package com.project.zzrk.repository.report;

import com.project.zzrk.model.report.ReportShift;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportShiftRepository
        extends JpaRepository<ReportShift, Integer> {

    @Query("SELECT rsh FROM ReportShift AS rsh ORDER BY rsh.id ASC")
    List<ReportShift> findAll();
}
