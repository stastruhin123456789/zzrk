package com.project.zzrk.repository.report;

import com.project.zzrk.model.report.ReportSubdivision;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportSubdivisionRepository
        extends JpaRepository<ReportSubdivision, Integer> {

    @Query("SELECT rsb FROM ReportSubdivision AS rsb " +
            "WHERE rsb.report.id= :reportId ORDER BY rsb.id ASC")
    List<ReportSubdivision> findByReportId(@Param("reportId") int reportId);
}
