package com.project.zzrk.repository.unittable;

import com.project.zzrk.model.unittable.UnitReportBox;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;








@Repository
public interface UnitReportBoxRepository
        extends JpaRepository<UnitReportBox, Integer> {

    @Query("SELECT CASE WHEN COUNT(urb) > 0 THEN true ELSE false END FROM UnitReportBox AS urb")
    boolean existsAll();

    List<UnitReportBox> findAllByOrderByIdAsc();

    boolean existsByDate(LocalDate date);

    UnitReportBox findByDate(LocalDate date);

    @Query("SELECT CASE WHEN COUNT(urb) > 0 THEN true ELSE false END FROM UnitReportBox AS urb " +
            "WHERE urb.date <= :reportDate AND urb.isActual = true")
    boolean existsByReportDate(@Param("reportDate") LocalDate reportDate);
}










