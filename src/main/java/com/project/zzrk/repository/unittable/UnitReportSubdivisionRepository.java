package com.project.zzrk.repository.unittable;

import com.project.zzrk.model.unittable.UnitReportSubdivision;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface UnitReportSubdivisionRepository
        extends JpaRepository<UnitReportSubdivision, Integer> {

    @Query("SELECT CASE WHEN COUNT(ursub) > 0 THEN true ELSE false END FROM UnitReportSubdivision AS ursub")
    boolean existsAll();

    List<UnitReportSubdivision> findAllByOrderByIdAsc();

    boolean existsByDate(LocalDate date);

    @Query("SELECT CASE WHEN COUNT(ursub) > 0 THEN true ELSE false END FROM UnitReportSubdivision AS ursub " +
            "WHERE ursub.date <= :reportDate AND ursub.isActual = true")
    boolean existsByReportDate(@Param("reportDate") LocalDate reportDate);

    UnitReportSubdivision findByDate(LocalDate date);
}
