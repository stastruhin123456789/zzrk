package com.project.zzrk.service;

import com.project.zzrk.model.Box;
import com.project.zzrk.util.response.ResponseStatus;

import java.util.List;

public interface BoxService {

    boolean existsAll();

    List<Box> findAll();

    Box findById(int id);

    boolean existsById(int id);

    ResponseStatus create(Box box);

    ResponseStatus update(Box box);
}
