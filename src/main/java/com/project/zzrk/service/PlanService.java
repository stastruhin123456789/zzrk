package com.project.zzrk.service;

import com.project.zzrk.dto.PlanDto;
import com.project.zzrk.model.Plan;
import com.project.zzrk.model.Subdivision;
import com.project.zzrk.util.response.ResponseStatus;

import java.time.LocalDate;
import java.util.List;

public interface PlanService {

    boolean existsUnitAll();
    boolean existsSubdivisionAll();
    boolean existsUnitReportSubdivisionAll();
    List<Subdivision> findSubdivisionAll();
    boolean existsAll();
    boolean existsByReportDate(LocalDate reportDate);
    List<Plan> findAll();
    boolean existsById(int id);
    Plan findById(int id);
    List<Plan> findByDateYearMonthAndSubdivisionId(PlanDto planDto);
    ResponseStatus create(PlanDto planDto, int step);
    ResponseStatus update(Plan plan);
}
