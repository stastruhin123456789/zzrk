package com.project.zzrk.service;

import com.project.zzrk.model.Shift;
import com.project.zzrk.util.response.ResponseStatus;

import java.util.List;

public interface ShiftService {

    boolean existsAll();

    List<Shift> findAll();

    boolean existsById(int id);

    Shift findById(int id);

    ResponseStatus create(Shift shift);

    ResponseStatus update(Shift shift);
}
