package com.project.zzrk.service;

import com.project.zzrk.model.Box;
import com.project.zzrk.model.Subdivision;
import com.project.zzrk.model.SubdivisionBox;
import com.project.zzrk.util.response.ResponseStatus;

import java.util.List;

public interface SubdivisionBoxService {

    boolean existsAll();
    boolean existsSubdivisionAll();
    boolean existsBoxAll();

    List<Subdivision> findSubdivisionAll();

    List<Box> findBoxAll();

    boolean existsById(int id);

    SubdivisionBox findById(int id);

    ResponseStatus create(SubdivisionBox subdivisionBox);

    ResponseStatus update(SubdivisionBox subdivisionBox);
}
