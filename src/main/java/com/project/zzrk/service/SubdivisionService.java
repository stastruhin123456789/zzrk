package com.project.zzrk.service;

import com.project.zzrk.model.Subdivision;
import com.project.zzrk.util.response.ResponseStatus;

import java.util.List;

public interface SubdivisionService {

    boolean existsAll();

    List<Subdivision> findAll();

    boolean existsById(int id);

    Subdivision findById(int id);

    ResponseStatus create(Subdivision subdivision);

    ResponseStatus update(Subdivision subdivision);
}
