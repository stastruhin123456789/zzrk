package com.project.zzrk.service;

import com.project.zzrk.model.Unit;
import com.project.zzrk.model.UnitConvert;
import com.project.zzrk.util.response.ResponseStatus;

import java.util.List;

public interface UnitConvertService {

    boolean existsAll();

    List<UnitConvert> findAll();

    boolean existsUnitAll();

    List<Unit> findUnitAll();

    ResponseStatus create(UnitConvert unitConvert);

    ResponseStatus update(UnitConvert unitConvert);

    boolean existsByDateAndFromUnitAndToUnit(UnitConvert unitConvert);

    UnitConvert findByDateAndFromUnitAndToUnit(UnitConvert unitConvert);

    boolean existsById(int id);

    UnitConvert findById(int id);
}
