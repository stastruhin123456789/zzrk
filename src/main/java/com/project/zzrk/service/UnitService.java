package com.project.zzrk.service;

import com.project.zzrk.model.Unit;
import com.project.zzrk.util.response.ResponseStatus;

import java.util.List;

public interface UnitService {

    boolean existsAll();

    List<Unit> findAll();

    ResponseStatus create(Unit unit);

    boolean existsById(int id);

    Unit findById(int id);

    ResponseStatus update(Unit unit);
}
