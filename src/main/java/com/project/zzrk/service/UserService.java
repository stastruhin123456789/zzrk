package com.project.zzrk.service;

import com.project.zzrk.model.User;
import com.project.zzrk.util.response.ResponseStatus;

import java.util.List;


public interface UserService {

    boolean existsAll();

    List<User> findAll();

    boolean existsById(int id);

    User findById(int id);

    ResponseStatus update(User userPost);

    ResponseStatus create(User userPost);
}



















