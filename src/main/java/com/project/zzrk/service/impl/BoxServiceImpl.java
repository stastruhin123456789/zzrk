package com.project.zzrk.service.impl;

import com.project.zzrk.model.Box;
import com.project.zzrk.repository.BoxRepository;
import com.project.zzrk.service.BoxService;
import com.project.zzrk.util.response.ResponseStatus;
import com.project.zzrk.util.response.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BoxServiceImpl implements BoxService {

    @Autowired
    private BoxRepository repository;

    @Override
    public boolean existsAll() {
        return repository.existsAll();
    }

    @Override
    public List<Box> findAll() {
        return repository.findAll();
    }

    @Override
    public Box findById(int id) {
        return repository.findById(id).orElseThrow();
    }

    @Override
    public boolean existsById(int id) {
        return repository.existsById(id);
    }

    @Override
    public ResponseStatus create(Box box) {
        var responseStatus = new ResponseStatus();
        if(repository.existsByName(box.getName())){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("name");
            responseStatus.setMessage("Дана назва існує.");
            return responseStatus;
        }
        repository.save(box);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }

    @Override
    public ResponseStatus update(Box box) {
        var responseStatus = new ResponseStatus();
        if(
                repository.existsByName(box.getName())
                        ^ findById(box.getId()).getName().equals(box.getName())
        ){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("name");
            responseStatus.setMessage("Дана назва існує.");
            return responseStatus;
        }
        repository.save(box);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }
}
