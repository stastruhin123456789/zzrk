package com.project.zzrk.service.impl;

import com.project.zzrk.dto.PlanDto;
import com.project.zzrk.model.Plan;
import com.project.zzrk.model.Subdivision;
import com.project.zzrk.repository.PlanRepository;
import com.project.zzrk.service.PlanService;
import com.project.zzrk.service.SubdivisionService;
import com.project.zzrk.service.UnitService;
import com.project.zzrk.service.unittable.table.UnitReportSubdivisionService;
import com.project.zzrk.util.response.ResponseStatus;
import com.project.zzrk.util.response.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;

@Service
public class PlanServiceImpl implements PlanService {
    @Autowired
    private PlanRepository repository;
    @Autowired
    private UnitService unitService;
    @Autowired
    private SubdivisionService subdivisionService;
    @Autowired
    private UnitReportSubdivisionService unitReportSubdivisionService;

    @Override
    public boolean existsUnitAll() {
        return unitService.existsAll();
    }

    @Override
    public boolean existsSubdivisionAll() {
        return subdivisionService.existsAll();
    }

    @Override
    public boolean existsUnitReportSubdivisionAll() {
        return unitReportSubdivisionService.existsAll();
    }

    @Override
    public List<Subdivision> findSubdivisionAll() {
        return subdivisionService.findAll();
    }

    @Override
    public boolean existsAll() {
        return repository.existsAll();
    }

    @Override
    public boolean existsByReportDate(LocalDate reportDate) {
        return repository.existsByReportDate(reportDate);
    }

    @Override
    public List<Plan> findAll() {
        return repository.findAll();
    }

    @Override
    public boolean existsById(int id) {
        return repository.existsById(id);
    }

    @Override
    public Plan findById(int id) {
        return repository.findById(id).orElseThrow();
    }

    public boolean existsByDateYearMonthAndSubdivisionId(PlanDto planDto) {
        YearMonth yearMonth = planDto.getDateYearMonth();
        return repository.existsByDateYearMonthAndSubdivisionId(
                yearMonth.getYear(), yearMonth.getMonthValue(),
                planDto.getSubdivisionId()
        );
    }

    @Override
    public List<Plan> findByDateYearMonthAndSubdivisionId(PlanDto planDto) {
        YearMonth yearMonth = planDto.getDateYearMonth();
        return repository.findByDateYearMonthAndSubdivisionId(
                yearMonth.getYear(), yearMonth.getMonthValue(),
                planDto.getSubdivisionId());
    }

    @Override
    public ResponseStatus create(PlanDto planDto, int step) {
        var responseStatus = new ResponseStatus();
        if (step == 1) {
            if (!unitReportSubdivisionService.existsByReportDate(planDto.getDateYearMonth().atEndOfMonth())){
                responseStatus.setStatus(Status.ERROR);
                responseStatus.putErrors("dateYearMonthNotification");
            }
            if (!subdivisionService.existsById(planDto.getSubdivisionId())) {
                responseStatus.setStatus(Status.ERROR);
                responseStatus.putErrors("subdivisionId");
                responseStatus.setMessage("Даного підрозділу не існує");
            }
            if (planDto.getDayValueForMonth() < 0) {
                responseStatus.setStatus(Status.ERROR);
                responseStatus.putErrors("dayValuePerMonth");
                responseStatus.setMessage("Неправильно введено значення плану на день за міcяць.");
            }
            if (responseStatus.isError()) {
                return responseStatus;
            }
            if (existsByDateYearMonthAndSubdivisionId(planDto)) {
                responseStatus.setStatus(Status.ERROR);
                responseStatus.putErrors("dateYearMonth");
                responseStatus.putErrors("subdivisionId");
                responseStatus.setMessage("По заданій даті записи вже існує.");
            }
            if (responseStatus.isError()) {
                return responseStatus;
            }
            repository.createPlan(
                    planDto.getDateYearMonth().atDay(1),
                    planDto.getSubdivisionId(),
                    planDto.getDayValueForMonth()
            );

        } else if (step == 2) {
            List<Plan> list = planDto.getListPlan();
            int n = list.size();
            for (int i = 0; i < n; i++) {
                if (list.get(i).getDayValue() < 0) {
                    responseStatus.setStatus(Status.ERROR);
                    responseStatus.putErrors(String.format("listPlan[%s].dayValue", i));
                    responseStatus.setMessage("Некоректне значення за " + list.get(i).getDate());
                }
            }
            if (responseStatus.isError()) {
                return responseStatus;
            }
            repository.saveAll(planDto.getListPlan());
        }
        repository.updatePlanMonthValue(
                planDto.getDateYearMonth().atDay(1),
                planDto.getSubdivisionId()
        );
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }

    @Override
    public ResponseStatus update(Plan plan) {
        var responseStatus = new ResponseStatus();
        if (plan.getDayValue() < 0) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("dayValue");
            responseStatus.setMessage("Неправильно введено значення плану на день.");
        }
        if (responseStatus.isError()) {
            return responseStatus;
        }
        repository.save(plan);
        repository.updatePlanMonthValue(plan.getDate(), plan.getSubdivision().getId());
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }

}
