package com.project.zzrk.service.impl;

import com.project.zzrk.model.Shift;
import com.project.zzrk.repository.ShiftRepository;
import com.project.zzrk.service.ShiftService;
import com.project.zzrk.util.response.ResponseStatus;
import com.project.zzrk.util.response.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShiftServiceImpl implements ShiftService {

    @Autowired
    private ShiftRepository repository;

    @Override
    public boolean existsAll() {
        return repository.existsAll();
    }

    @Override
    public List<Shift> findAll() {
        return repository.findAll();
    }

    @Override
    public boolean existsById(int id) {
        return repository.existsById(id);
    }

    @Override
    public Shift findById(int id) {
        return repository.findById(id).orElseThrow();
    }

    @Override
    public ResponseStatus create(Shift shiftPost) {
        var responseStatus = new ResponseStatus();
        if(repository.existsByName(shiftPost.getName())){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("name");
            responseStatus.setMessage("Дана назва існує.");
            return responseStatus;
        }
        repository.save(shiftPost);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }

    @Override
    public ResponseStatus update(Shift shiftPost) {
        var responseStatus = new ResponseStatus();
        if(
                repository.existsByName(shiftPost.getName())
                        ^ findById(shiftPost.getId()).getName()
                        .equals(shiftPost.getName()
                        )
        ){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("name");
            responseStatus.setMessage("Дана назва існує.");
            return responseStatus;
        }
        repository.save(shiftPost);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }

}
