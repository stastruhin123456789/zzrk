package com.project.zzrk.service.impl;

import com.project.zzrk.model.Box;
import com.project.zzrk.model.Subdivision;
import com.project.zzrk.model.SubdivisionBox;
import com.project.zzrk.repository.SubdivisionBoxRepository;
import com.project.zzrk.service.BoxService;
import com.project.zzrk.service.SubdivisionBoxService;
import com.project.zzrk.service.SubdivisionService;
import com.project.zzrk.util.response.ResponseStatus;
import com.project.zzrk.util.response.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubdivisionBoxServiceImpl implements SubdivisionBoxService {
    @Autowired
    private SubdivisionBoxRepository repository;
    @Autowired
    private SubdivisionService subdivisionService;
    @Autowired
    private BoxService boxService;

    @Override
    public boolean existsAll() {
        return repository.existsAll();
    }

    @Override
    public boolean existsSubdivisionAll() {
        return subdivisionService.existsAll();
    }

    @Override
    public boolean existsBoxAll() {
        return boxService.existsAll();
    }

    @Override
    public List<Subdivision> findSubdivisionAll() {
        return subdivisionService.findAll();
    }

    @Override
    public List<Box> findBoxAll() {
        return boxService.findAll();
    }

    @Override
    public boolean existsById(int id) {
        return repository.existsById(id);
    }

    @Override
    public SubdivisionBox findById(int id) {
        return repository.findById(id).orElseThrow();
    }

    private boolean existsBySubdivisionAndBox(SubdivisionBox subdivisionBox) {
        return repository.existsBySubdivisionAndBox(subdivisionBox.getSubdivision(), subdivisionBox.getBox());
    }

    private SubdivisionBox findBySubdivisionAndBox(SubdivisionBox subdivisionBox) {
        return repository.findBySubdivisionAndBox(
                subdivisionBox.getSubdivision(),
                subdivisionBox.getBox()
        ).orElseThrow();
    }

    @Override
    public ResponseStatus create(SubdivisionBox subdivisionBox) {
        var responseStatus = new ResponseStatus();
        if (subdivisionBox.getSubdivision() == null) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("subdivision");
            responseStatus.setMessage("Неправильно обраний підрозділ.");
        }
        if (subdivisionBox.getBox() == null) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("box");
            responseStatus.setMessage("Неправильно обрана камера.");
        }
        if (responseStatus.isError()) {
            return responseStatus;
        }
        if (existsBySubdivisionAndBox(subdivisionBox)) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("subdivision");
            responseStatus.putErrors("box");
            responseStatus.setMessage("Даний запис вже існує.");
        }
        if (responseStatus.isError()) {
            return responseStatus;
        }
        repository.save(subdivisionBox);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }

    @Override
    public ResponseStatus update(SubdivisionBox subdivisionBox) {
        var responseStatus = new ResponseStatus();
        if (subdivisionBox.getSubdivision() == null) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("subdivision");
            responseStatus.setMessage("Неправильно обраний підрозділ.");
        }
        if (subdivisionBox.getBox() == null) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("box");
            responseStatus.setMessage("Неправильно обрана камера.");
        }
        if (responseStatus.isError()) {
            return responseStatus;
        }
        if (existsBySubdivisionAndBox(subdivisionBox))
            if( !(findBySubdivisionAndBox(subdivisionBox).getId() == subdivisionBox.getId()) ){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("subdivision");
            responseStatus.putErrors("box");
            responseStatus.setMessage("Даний запис вже існує.");
        }
        if (responseStatus.isError()) {
            return responseStatus;
        }
        repository.save(subdivisionBox);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }

}
