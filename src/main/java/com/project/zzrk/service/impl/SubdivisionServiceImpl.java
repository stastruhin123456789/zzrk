package com.project.zzrk.service.impl;

import com.project.zzrk.model.Subdivision;
import com.project.zzrk.repository.SubdivisionRepository;
import com.project.zzrk.service.SubdivisionService;
import com.project.zzrk.util.response.ResponseStatus;
import com.project.zzrk.util.response.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubdivisionServiceImpl implements SubdivisionService {

    @Autowired
    private SubdivisionRepository repository;

    @Override
    public boolean existsAll() {
        return repository.existsAll();
    }

    @Override
    public List<Subdivision> findAll() {
        return repository.findAll();
    }

    @Override
    public boolean existsById(int id) {
        return repository.existsById(id);
    }

    @Override
    public Subdivision findById(int id) {
        return repository.findById(id).orElseThrow();
    }

    @Override
    public ResponseStatus create(Subdivision subdivision) {
        var responseStatus = new ResponseStatus();
        if(repository.existsByName(subdivision.getName())){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("name");
            responseStatus.setMessage("Дана назва існує.");
            return responseStatus;
        }
        repository.save(subdivision);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }

    @Override
    public ResponseStatus update(Subdivision subdivision) {
        var responseStatus = new ResponseStatus();
        if(
                repository.existsByName(subdivision.getName())
                        ^ findById(subdivision.getId()).getName().equals(subdivision.getName())
        ){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("name");
            responseStatus.setMessage("Дана назва існує.");
            return responseStatus;
        }
        repository.save(subdivision);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }
}
