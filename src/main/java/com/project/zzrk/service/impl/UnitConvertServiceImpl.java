package com.project.zzrk.service.impl;

import com.project.zzrk.model.Unit;
import com.project.zzrk.model.UnitConvert;
import com.project.zzrk.repository.UnitConvertRepository;
import com.project.zzrk.service.UnitConvertService;
import com.project.zzrk.service.UnitService;
import com.project.zzrk.util.response.ResponseStatus;
import com.project.zzrk.util.response.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnitConvertServiceImpl implements UnitConvertService {

    @Autowired
    private UnitConvertRepository repository;

    @Autowired
    private UnitService unitService;

    @Override
    public boolean existsAll() {
        return repository.existsAll();
    }

    @Override
    public List<UnitConvert> findAll() {
        return repository.findAll();
    }

    @Override
    public boolean existsUnitAll() {
        return unitService.existsAll();
    }

    @Override
    public List<Unit> findUnitAll() {
        return unitService.findAll();
    }

    @Override
    public boolean existsByDateAndFromUnitAndToUnit(UnitConvert unitConvert){
        return repository.existsByDateAndFromUnitAndToUnit(
                unitConvert.getDate(),
                unitConvert.getFromUnit(),
                unitConvert.getToUnit()
        );
    }

    @Override
    public UnitConvert findByDateAndFromUnitAndToUnit(UnitConvert unitConvert){
        return repository.findByDateAndFromUnitAndToUnit(
                unitConvert.getDate(),
                unitConvert.getFromUnit(),
                unitConvert.getToUnit()
        ).orElseThrow();
    }

    @Override
    public boolean existsById(int id) {
        return repository.existsById(id);
    }

    @Override
    public UnitConvert findById(int id) {
        return repository.findById(id).orElseThrow();
    }

    @Override
    public ResponseStatus update(UnitConvert unitConvert) {
        var responseStatus = new ResponseStatus();
        if (unitConvert.getFromUnit() == null) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("fromUnit");
            responseStatus.setMessage("Неправильно обрана одиниця виміру від якої конвертувати.");
        }
        if (unitConvert.getToUnit() == null) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("toUnit");
            responseStatus.setMessage("Неправильно обрана одиниця виміру до якої конвертувати.");
        }
        if(!(unitConvert.getCoefficient() > 0)){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("coefficient");
            responseStatus.setMessage("Неправильно введено значення коефіцієнта конвертації.");
        }
        if(responseStatus.isError()){
            return responseStatus;
        }
        if(existsByDateAndFromUnitAndToUnit(unitConvert))
            if( !(findByDateAndFromUnitAndToUnit(unitConvert).getId() == unitConvert.getId()) ){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("date");
            responseStatus.setMessage("Даний запис вже існує.");
        }
        if(unitConvert.getFromUnit()==unitConvert.getToUnit())
        {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("fromUnit");
            responseStatus.putErrors("toUnit");
            responseStatus.setMessage("Обрані однакові одиниці вимірування.");
        }
        if(responseStatus.isError()){
            return responseStatus;
        }
        repository.save(unitConvert);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }

    @Override
    public ResponseStatus create(UnitConvert unitConvert) {
        var responseStatus = new ResponseStatus();
        if (unitConvert.getFromUnit() == null) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("fromUnit");
            responseStatus.setMessage("Неправильно обрана одиниця виміру від якої конвертувати.");
        }
        if (unitConvert.getToUnit() == null) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("toUnit");
            responseStatus.setMessage("Неправильно обрана одиниця виміру до якої конвертувати.");
        }
        if(existsByDateAndFromUnitAndToUnit(unitConvert)){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("date");
            responseStatus.setMessage("Даний запис вже існує.");
        }
        if(unitConvert.getFromUnit()==unitConvert.getToUnit())
        {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("fromUnit");
            responseStatus.putErrors("toUnit");
            responseStatus.setMessage("Обрані однакові одиниці вимірування.");
        }
        if(!(unitConvert.getCoefficient() > 0)){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("coefficient");
            responseStatus.setMessage("Неправильно введено значення коефіцієнта конвертації.");
        }
        if(responseStatus.isError()){
            return responseStatus;
        }
        repository.save(unitConvert);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }
}
