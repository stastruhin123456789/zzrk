package com.project.zzrk.service.impl;

import com.project.zzrk.model.Unit;
import com.project.zzrk.repository.UnitRepository;
import com.project.zzrk.service.UnitService;
import com.project.zzrk.util.response.ResponseStatus;
import com.project.zzrk.util.response.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnitServiceImpl implements UnitService {

    @Autowired
    UnitRepository repository;

    @Override
    public boolean existsAll() {
        return repository.existsAll();
    }

    @Override
    public List<Unit> findAll() {
        return repository.findAll();
    }

    @Override
    public boolean existsById(int id) {
        return repository.existsById(id);
    }

    @Override
    public Unit findById(int id) {
        return repository.findById(id).orElseThrow();
    }

    @Override
    public ResponseStatus create(Unit unitPost) {
        var responseStatus = new ResponseStatus();
        if(repository.existsByName(unitPost.getName())){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("name");
            responseStatus.setMessage("Дана назва існує.");
            return responseStatus;
        }
        repository.save(unitPost);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }

    @Override
    public ResponseStatus update(Unit unitPost) {
        var responseStatus = new ResponseStatus();
        if(
                repository.existsByName(unitPost.getName())
                        ^ findById(unitPost.getId()).getName().equals(unitPost.getName())
        ){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("name");
            responseStatus.setMessage("Дана назва існує.");
            return responseStatus;
        }
        repository.save(unitPost);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }


}
