package com.project.zzrk.service.impl;

import com.project.zzrk.model.User;
import com.project.zzrk.repository.UserRepository;
import com.project.zzrk.service.UserService;
import com.project.zzrk.util.response.ResponseStatus;
import com.project.zzrk.util.response.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Override
    public boolean existsAll() {
        return repository.existsAll();
    }

    @Override
    public List<User> findAll() {
        return repository.findAll();
    }

    @Override
    public boolean existsById(int id) {
        return repository.existsById(id);
    }

    @Override
    public User findById(int id) {
        return repository.findById(id).orElseThrow();
    }

    @Override
    public ResponseStatus update(User userPost) {
        var responseStatus = new ResponseStatus();
        if(
                repository.existsByLogin(userPost.getLogin())
                        ^ findById(userPost.getId()).getLogin().equals(userPost.getLogin()
        )
        ){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("login");
            responseStatus.setMessage("Даний логін існує.");
            return responseStatus;
        }
        repository.save(userPost);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }

    @Override
    public ResponseStatus create(User userPost) {
        var responseStatus = new ResponseStatus();
        if(repository.existsByLogin(userPost.getLogin())){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("login");
            responseStatus.setMessage("Даний логін існує.");
            return responseStatus;
        }
        repository.save(userPost);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }
}
