package com.project.zzrk.service.impl.report;

import com.project.zzrk.model.Box;
import com.project.zzrk.model.report.ReportBox;
import com.project.zzrk.model.report.ReportShift;
import com.project.zzrk.repository.report.ReportBoxRepository;
import com.project.zzrk.service.BoxService;
import com.project.zzrk.service.report.ReportBoxService;
import com.project.zzrk.service.report.ReportService;
import com.project.zzrk.service.report.ReportShiftService;
import com.project.zzrk.service.unittable.table.UnitReportBoxService;
import com.project.zzrk.util.response.ResponseStatus;
import com.project.zzrk.util.response.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ReportBoxServiceImpl implements ReportBoxService {
    @Autowired
    private ReportBoxRepository repository;
    @Autowired
    private ReportShiftService reportShiftService;
    @Autowired
    private ReportService reportService;
    @Autowired
    private BoxService boxService;
    @Autowired
    private UnitReportBoxService unitReportBoxService;
    @Override
    public boolean existsById(int id) {
        return repository.existsById(id);
    }
    @Override
    public boolean existsUnitReportBoxByReportDate(LocalDate reportDate) {
        return unitReportBoxService.existsByReportDate(reportDate);
    }
    @Override
    public ReportBox findById(int id) {
        return repository.findById(id).orElseThrow();
    }
    @Override
    public List<ReportBox> findAll() {
        return repository.findAll();
    }
    @Override
    public boolean existsReportShiftByReportShiftId(int reportShiftId) {
        return reportShiftService.existsById(reportShiftId);
    }
    @Override
    public boolean existsByReportShiftId(int reportShiftId) {
        return repository.existsByReportShiftId(reportShiftId);
    }
    @Override
    public List<ReportBox> findByReportShiftId(int reportShiftId) {
        return repository.findByReportShiftId(reportShiftId);
    }
    @Override
    public boolean existsReportByReportId(int reportId) {
        return reportService.existsById(reportId);
    }
    @Override
    public LocalDate findReportDateByReportId(int reportId) {
        return reportService.findById(reportId).getDate();
    }
    @Override
    public ReportShift findReportShiftByReportShiftId(int reportShiftId) {
        return reportShiftService.findById(reportShiftId);
    }
    @Override
    public ResponseStatus create(int reportShiftId, int boxId, int boxValue) {
        var responseStatus = new ResponseStatus();
        if (!boxService.existsById(boxId) || repository.existsByReportShiftIdAndBoxId(reportShiftId, boxId)) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("boxId");
            responseStatus.setMessage("Неправильно обрана камера.");
        }
        if (!(boxValue > 0)) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("boxValue");
            responseStatus.setMessage("Неправильно введено значення видобутої руди.");
        }
        if (responseStatus.isError()) {
            return responseStatus;
        }

        ReportBox reportBox = new ReportBox();
        ReportShift reportShift = reportShiftService.findById(reportShiftId);
        Box box = boxService.findById(boxId);
        reportBox.setBox(box);
        reportBox.setReportShift(reportShift);
        reportBox.setValue(boxValue);
        repository.save(reportBox);

        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }
    @Override
    public ResponseStatus update(ReportBox reportBox) {
        var responseStatus = new ResponseStatus();
        if (!(reportBox.getValue() > 0)) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("value");
            responseStatus.setMessage("Неправильно введено значення видобутої руди.");
        }
        if (responseStatus.isError()) {
            return responseStatus;
        }
        ReportBox reportBoxSave = findById(reportBox.getId());
        reportBoxSave.setValue(reportBox.getValue());
        repository.save(reportBoxSave);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }
}
