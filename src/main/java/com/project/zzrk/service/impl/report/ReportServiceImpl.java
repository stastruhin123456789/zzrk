package com.project.zzrk.service.impl.report;

import com.project.zzrk.model.report.Report;
import com.project.zzrk.repository.report.ReportRepository;
import com.project.zzrk.service.*;
import com.project.zzrk.service.report.ReportService;
import com.project.zzrk.service.unittable.table.UnitReportSubdivisionService;
import com.project.zzrk.util.response.ResponseStatus;
import com.project.zzrk.util.response.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ReportRepository repository;
    @Autowired
    private SubdivisionService subdivisionService;
    @Autowired
    private BoxService boxService;
    @Autowired
    private SubdivisionBoxService subdivisionBoxService;
    @Autowired
    private ShiftService shiftService;
    @Autowired
    private UnitService unitService;
    @Autowired
    private UnitConvertService unitConvertService;
    @Autowired
    private UnitReportSubdivisionService unitReportSubdivisionService;
    @Autowired
    private PlanService planService;


    @Override
    public boolean existsAll() {
        return repository.existsAll();
    }
    @Override
    public boolean existsSubdivisionAll() {
        return subdivisionService.existsAll();
    }
    @Override
    public boolean existsBoxAll() {
        return boxService.existsAll();
    }
    @Override
    public boolean existsSubdivisionBoxAll() {
        return subdivisionBoxService.existsAll();
    }
    @Override
    public boolean existsShiftAll() {
        return shiftService.existsAll();
    }
    @Override
    public boolean existsUnitAll() {
        return unitService.existsAll();
    }
    @Override
    public boolean existsUnitConvertAll() {
        return unitConvertService.existsAll();
    }
    @Override
    public boolean existsUnitReportSubdivisionAll() {
        return unitReportSubdivisionService.existsAll();
    }
    @Override
    public boolean existsPlanAll() {
        return planService.existsAll();
    }
    @Override
    public List<Report> findAll() {
        return repository.findAll();
    }
    @Override
    public boolean existsById(int id) {
        return repository.existsById(id);
    }
    @Override
    public Report findById(int id) {
        return repository.findById(id).orElseThrow();
    }
    @Override
    public ResponseStatus create() {
        var responseStatus = new ResponseStatus();
        LocalDate reportDate;
        if (existsAll()) {
            reportDate = repository.findFirstByOrderByDateDesc().orElseThrow().getDate();
            reportDate = reportDate.plusDays(1);
        } else {
            reportDate = LocalDate.now();
        }
        if(!unitReportSubdivisionService.existsByReportDate(reportDate)){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("unitReportSubdivisionNotification");
            responseStatus.setMessage("Одиниць вимірювання для Звіту за даною датою немає.");
        }
        if(!planService.existsByReportDate(reportDate)){
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("planNotification");
            responseStatus.setMessage("Плана за даною датою немає.");
        }
        if (responseStatus.isError()) {
            return responseStatus;
        }
        Report report = new Report();
        report.setDate(reportDate);
        repository.save(report);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }

}
