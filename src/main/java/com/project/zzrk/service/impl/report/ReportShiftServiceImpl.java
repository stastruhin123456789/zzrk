package com.project.zzrk.service.impl.report;

import com.project.zzrk.model.report.ReportShift;
import com.project.zzrk.repository.report.ReportShiftRepository;
import com.project.zzrk.service.report.ReportShiftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportShiftServiceImpl implements ReportShiftService {

    @Autowired
    private ReportShiftRepository repository;

    @Override
    public List<ReportShift> findAll() {
        return repository.findAll();
    }

    @Override
    public boolean existsById(int id) {
        return repository.existsById(id);
    }

    @Override
    public ReportShift findById(int id) {
        return repository.findById(id).orElseThrow();
    }
}
