package com.project.zzrk.service.impl.report;

import com.project.zzrk.model.report.ReportSubdivision;
import com.project.zzrk.repository.report.ReportSubdivisionRepository;
import com.project.zzrk.service.report.ReportService;
import com.project.zzrk.service.report.ReportSubdivisionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class ReportSubdivisionServiceImpl implements ReportSubdivisionService {

    @Autowired
    private ReportSubdivisionRepository repository;

    @Autowired
    private ReportService reportService;

    @Override
    public List<ReportSubdivision> findAll() {
        return repository.findAll();
    }

    @Override
    public boolean existsReportByReportId(int reportId) {
        return reportService.existsById(reportId);
    }

    @Override
    public List<ReportSubdivision> findByReportId(int reportId) {
        return repository.findByReportId(reportId);
    }

    @Override
    public LocalDate findReportDateByReportId(int reportId) {
        return reportService.findById(reportId).getDate();
    }
}
