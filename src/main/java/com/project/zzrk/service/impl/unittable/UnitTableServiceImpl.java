package com.project.zzrk.service.impl.unittable;

import com.project.zzrk.model.Unit;
import com.project.zzrk.model.unittable.UnitReportBox;
import com.project.zzrk.model.unittable.UnitReportSubdivision;
import com.project.zzrk.service.UnitService;
import com.project.zzrk.service.unittable.table.UnitReportBoxService;
import com.project.zzrk.service.unittable.table.UnitReportSubdivisionService;
import com.project.zzrk.service.unittable.UnitTableService;
import com.project.zzrk.util.response.ResponseStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnitTableServiceImpl implements UnitTableService {
    @Autowired
    private UnitService unitService;
    @Autowired
    private UnitReportSubdivisionService unitReportSubdivisionService;
    @Autowired
    private UnitReportBoxService unitReportBoxService;

    @Override
    public List<Unit> findUnitAll() {
        return unitService.findAll();
    }

    @Override
    public boolean existsUnitAll() {
        return unitService.existsAll();
    }

    @Override
    public boolean existsUnitReportSubdivisionAll() {
        return unitReportSubdivisionService.existsAll();
    }
    @Override
    public List<UnitReportSubdivision> findUnitReportSubdivisionAll() {
        return unitReportSubdivisionService.findAll();
    }
    @Override
    public boolean existsUnitReportSubdivisionById(int id) {
        return unitReportSubdivisionService.existsById(id);
    }
    @Override
    public UnitReportSubdivision findUnitReportSubdivisionById(int id) {
        return unitReportSubdivisionService.findById(id);
    }
    @Override
    public ResponseStatus create(UnitReportSubdivision unitReportSubdivision) {
        return unitReportSubdivisionService.create(unitReportSubdivision);
    }
    @Override
    public ResponseStatus update(UnitReportSubdivision unitReportSubdivision) {
        return unitReportSubdivisionService.update(unitReportSubdivision);
    }

    @Override
    public boolean existsUnitReportBoxAll() {
        return unitReportBoxService.existsAll();
    }
    @Override
    public List<UnitReportBox> findUnitReportBoxAll() {
        return unitReportBoxService.findAll();
    }
    @Override
    public boolean existsUnitReportBoxById(int id) {
        return unitReportBoxService.existsById(id);
    }
    @Override
    public UnitReportBox findUnitReportBoxById(int id) {
        return unitReportBoxService.findById(id);
    }
    @Override
    public ResponseStatus create(UnitReportBox unitReportBox) {
        return unitReportBoxService.create(unitReportBox);
    }
    @Override
    public ResponseStatus update(UnitReportBox unitReportBox) {
        return unitReportBoxService.update(unitReportBox);
    }
}
