package com.project.zzrk.service.impl.unittable.table;

import com.project.zzrk.model.unittable.UnitReportBox;
import com.project.zzrk.repository.unittable.UnitReportBoxRepository;
import com.project.zzrk.service.unittable.table.UnitReportBoxService;
import com.project.zzrk.util.response.ResponseStatus;
import com.project.zzrk.util.response.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class UnitReportBoxServiceImpl implements UnitReportBoxService {

    @Autowired
    private UnitReportBoxRepository reportBoxRepository;

    @Override
    public boolean existsAll() {
        return reportBoxRepository.existsAll();
    }

    @Override
    public boolean existsByReportDate(LocalDate reportDate) {
        return reportBoxRepository.existsByReportDate(reportDate);
    }

    @Override
    public List<UnitReportBox> findAll() {
        return reportBoxRepository.findAllByOrderByIdAsc();
    }

    @Override
    public boolean existsById(int id) {
        return reportBoxRepository.existsById(id);
    }

    @Override
    public UnitReportBox findById(int id) {
        return reportBoxRepository.findById(id).orElseThrow();
    }

    public boolean existsByDate(UnitReportBox unitReportBox) {
        return reportBoxRepository.existsByDate(unitReportBox.getDate());
    }

    public UnitReportBox findByDate(UnitReportBox unitReportBox) {
        return reportBoxRepository.findByDate(unitReportBox.getDate());
    }

    @Override
    public ResponseStatus create(UnitReportBox unitReportBox) {
        var responseStatus = new ResponseStatus();
        if (unitReportBox.getUnit() == null) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("unit");
            responseStatus.setMessage("Неправильно обрана одиниця вимірування.");
        }
        if (existsByDate(unitReportBox)) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("date");
            responseStatus.setMessage("Даний запис вже існує.");
        }
        if (responseStatus.isError()) {
            return responseStatus;
        }
        reportBoxRepository.save(unitReportBox);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }

    @Override
    public ResponseStatus update(UnitReportBox unitReportBox) {
        var responseStatus = new ResponseStatus();
        if (unitReportBox.getUnit() == null) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("unit");
            responseStatus.setMessage("Неправильно обрана одиниця вимірування.");
        }
        if (existsByDate(unitReportBox))
            if (!(findByDate(unitReportBox).getId() == unitReportBox.getId())) {
                responseStatus.setStatus(Status.ERROR);
                responseStatus.putErrors("date");
                responseStatus.setMessage("Даний запис вже існує.");
            }
        if (responseStatus.isError()) {
            return responseStatus;
        }
        reportBoxRepository.save(unitReportBox);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }
}
