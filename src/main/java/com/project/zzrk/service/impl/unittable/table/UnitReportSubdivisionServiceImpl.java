package com.project.zzrk.service.impl.unittable.table;

import com.project.zzrk.model.unittable.UnitReportSubdivision;
import com.project.zzrk.repository.unittable.UnitReportSubdivisionRepository;
import com.project.zzrk.service.unittable.table.UnitReportSubdivisionService;
import com.project.zzrk.util.response.ResponseStatus;
import com.project.zzrk.util.response.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class UnitReportSubdivisionServiceImpl implements UnitReportSubdivisionService {

    @Autowired
    private UnitReportSubdivisionRepository reportSubdivisionRepository;

    @Override
    public boolean existsAll() {
        return reportSubdivisionRepository.existsAll();
    }

    @Override
    public boolean existsByReportDate(LocalDate reportDate) {
        return reportSubdivisionRepository.existsByReportDate(reportDate);
    }

    @Override
    public List<UnitReportSubdivision> findAll() {
        return reportSubdivisionRepository.findAllByOrderByIdAsc();
    }

    @Override
    public boolean existsById(int id) {
        return reportSubdivisionRepository.existsById(id);
    }

    @Override
    public UnitReportSubdivision findById(int id) {
        return reportSubdivisionRepository.findById(id).orElseThrow();
    }

    public boolean existsByDate(UnitReportSubdivision unitReportSubdivision) {
        return reportSubdivisionRepository.existsByDate(unitReportSubdivision.getDate());
    }

    public UnitReportSubdivision findByDate(UnitReportSubdivision unitReportSubdivision) {
        return reportSubdivisionRepository.findByDate(unitReportSubdivision.getDate());
    }

    @Override
    public ResponseStatus create(UnitReportSubdivision unitReportSubdivision) {
        var responseStatus = new ResponseStatus();
        if (unitReportSubdivision.getUnit() == null) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("unit");
            responseStatus.setMessage("Неправильно обрана одиниця вимірування.");
        }
        if (existsByDate(unitReportSubdivision)) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("date");
            responseStatus.setMessage("Даний запис вже існує.");
        }
        if (responseStatus.isError()) {
            return responseStatus;
        }
        reportSubdivisionRepository.save(unitReportSubdivision);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }

    @Override
    public ResponseStatus update(UnitReportSubdivision unitReportSubdivision) {
        var responseStatus = new ResponseStatus();
        if (unitReportSubdivision.getUnit() == null) {
            responseStatus.setStatus(Status.ERROR);
            responseStatus.putErrors("unit");
            responseStatus.setMessage("Неправильно обрана одиниця вимірування.");
        }
        if (existsByDate(unitReportSubdivision))
            if (!(findByDate(unitReportSubdivision).getId() == unitReportSubdivision.getId())) {
                responseStatus.setStatus(Status.ERROR);
                responseStatus.putErrors("date");
                responseStatus.setMessage("Даний запис вже існує.");
            }
        if (responseStatus.isError()) {
            return responseStatus;
        }
        reportSubdivisionRepository.save(unitReportSubdivision);
        responseStatus.setStatus(Status.SUCCESS);
        return responseStatus;
    }
}
