package com.project.zzrk.service.report;

import com.project.zzrk.model.report.ReportBox;
import com.project.zzrk.model.report.ReportShift;
import com.project.zzrk.util.response.ResponseStatus;

import java.time.LocalDate;
import java.util.List;

public interface ReportBoxService {

    boolean existsById(int id);
    boolean existsUnitReportBoxByReportDate(LocalDate reportDate);
    ReportBox findById(int id);
    List<ReportBox> findAll();
    boolean existsReportShiftByReportShiftId(int reportShiftId);
    boolean existsByReportShiftId(int reportShiftId);
    List<ReportBox> findByReportShiftId(int reportShiftId);
    boolean existsReportByReportId(int reportId);
    LocalDate findReportDateByReportId(int reportId);
    ReportShift findReportShiftByReportShiftId(int reportShiftId);
    ResponseStatus create(int reportShiftId, int boxId, int boxValue);
    ResponseStatus update(ReportBox reportBox);
}
