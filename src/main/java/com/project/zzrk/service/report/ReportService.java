package com.project.zzrk.service.report;


import com.project.zzrk.model.report.Report;
import com.project.zzrk.util.response.ResponseStatus;

import java.util.List;

public interface ReportService {

    boolean existsAll();
    boolean existsSubdivisionAll();
    boolean existsBoxAll();
    boolean existsSubdivisionBoxAll();
    boolean existsShiftAll();
    boolean existsUnitAll();
    boolean existsUnitConvertAll();
    boolean existsUnitReportSubdivisionAll();
    boolean existsPlanAll();

    List<Report> findAll();

    boolean existsById(int id);

    Report findById(int id);

    ResponseStatus create();
}
