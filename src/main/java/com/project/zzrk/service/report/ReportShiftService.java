package com.project.zzrk.service.report;

import com.project.zzrk.model.report.ReportShift;

import java.util.List;

public interface ReportShiftService {
    List<ReportShift> findAll();

    boolean existsById(int id);

    ReportShift findById(int id);
}
