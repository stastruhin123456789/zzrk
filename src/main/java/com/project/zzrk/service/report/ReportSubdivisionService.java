package com.project.zzrk.service.report;

import com.project.zzrk.model.report.ReportSubdivision;

import java.time.LocalDate;
import java.util.List;

public interface ReportSubdivisionService {
    List<ReportSubdivision> findAll();

    boolean existsReportByReportId(int reportId);

    List<ReportSubdivision> findByReportId(int reportId);

    LocalDate findReportDateByReportId(int reportId);
}
