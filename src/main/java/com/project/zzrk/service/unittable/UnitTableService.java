package com.project.zzrk.service.unittable;

import com.project.zzrk.model.Unit;
import com.project.zzrk.model.unittable.UnitReportBox;
import com.project.zzrk.model.unittable.UnitReportSubdivision;
import com.project.zzrk.util.response.ResponseStatus;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface UnitTableService {

    List<Unit> findUnitAll();
    boolean existsUnitAll();

    boolean existsUnitReportSubdivisionAll();
    List<UnitReportSubdivision> findUnitReportSubdivisionAll();
    boolean existsUnitReportSubdivisionById(int id);
    UnitReportSubdivision findUnitReportSubdivisionById(int id);
    ResponseStatus create(UnitReportSubdivision unitReportSubdivision);
    ResponseStatus update(UnitReportSubdivision unitReportSubdivision);

    boolean existsUnitReportBoxAll();
    List<UnitReportBox> findUnitReportBoxAll();
    boolean existsUnitReportBoxById(int id);
    UnitReportBox findUnitReportBoxById(int id);
    ResponseStatus create(UnitReportBox unitReportBox);
    ResponseStatus update(UnitReportBox unitReportBox);
}
