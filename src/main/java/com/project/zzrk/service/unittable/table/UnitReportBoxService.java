package com.project.zzrk.service.unittable.table;

import com.project.zzrk.model.unittable.UnitReportBox;
import com.project.zzrk.util.response.ResponseStatus;

import java.time.LocalDate;
import java.util.List;

public interface UnitReportBoxService {

    boolean existsAll();

    boolean existsByReportDate(LocalDate reportDate);

    List<UnitReportBox> findAll();

    boolean existsById(int id);

    UnitReportBox findById(int id);

    ResponseStatus create(UnitReportBox unitReportBox);

    ResponseStatus update(UnitReportBox unitReportBox);
}
