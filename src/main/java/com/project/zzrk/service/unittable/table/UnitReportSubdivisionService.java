package com.project.zzrk.service.unittable.table;

import com.project.zzrk.model.unittable.UnitReportSubdivision;
import com.project.zzrk.util.response.ResponseStatus;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;

public interface UnitReportSubdivisionService {

    boolean existsAll();

    boolean existsByReportDate(LocalDate reportDate);

    List<UnitReportSubdivision> findAll();

    boolean existsById(int id);

    UnitReportSubdivision findById(int id);

    ResponseStatus create(UnitReportSubdivision unitReportSubdivision);

    ResponseStatus update(UnitReportSubdivision unitReportSubdivision);
}
