package com.project.zzrk.util.response;

import java.util.Map;
import java.util.HashMap;


public class ResponseStatus {
    private Status status;
    private String message;
    private final Map<String, Boolean> errors;

    public ResponseStatus() {
        message = "";
        errors = new HashMap<>();
    }

    public ResponseStatus(Status status) {
        this();
        setStatus(status);
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public boolean isError() {
        return status == Status.ERROR;
    }

    public boolean isSuccess() {
        return status == Status.SUCCESS;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message += " " + message;
    }

    public Map<String, Boolean> getErrors() {
        return errors;
    }

    public void putErrors(String s) {
        errors.put(s, true);
    }
}
