package com.project.zzrk.validator;

import com.project.zzrk.service.UnitConvertService;
import com.project.zzrk.util.response.ResponseStatus;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class UnitConvertValidator implements Validator{

    UnitConvertService service;
    ResponseStatus responseStatus;

    @Override
    public boolean isValid() {
        return false;
    }
}
