package com.project.zzrk.validator;

public interface Validator {
    boolean isValid();
}
