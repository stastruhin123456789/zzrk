-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 25 2020 г., 18:21
-- Версия сервера: 5.7.25
-- Версия PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `zzrk`
--

DELIMITER $$
--
-- Процедуры
--
CREATE DEFINER=`root`@`%` PROCEDURE `create_plan` (IN `dateStart` DATE, IN `subdivisionId` INT UNSIGNED, IN `dayValueForMonth` INT UNSIGNED)  BEGIN
    DECLARE dateEnd DATE;
    SET dateEnd =  LAST_DAY(dateStart);
    WHILE dateStart <= dateEnd DO
    	IF dayValueForMonth = 0 THEN
        	INSERT INTO plans (plan_date, subdivision_id) VALUES(dateStart, subdivisionId);
        ELSE
        	INSERT INTO plans (plan_date, subdivision_id, plan_day_value) VALUES(dateStart,subdivisionId, dayValueForMonth);
        END IF;
        SET dateStart = DATE_ADD(dateStart, INTERVAL 1 DAY);
     
    END WHILE;
END$$

CREATE DEFINER=`root`@`%` PROCEDURE `update_plan_month_value` (IN `dateStart` DATE, IN `subdivisionId` INT UNSIGNED)  BEGIN
    DECLARE dateEnd DATE;
    DECLARE summ INT DEFAULT 0;
    DECLARE planDayValue INT;
    DECLARE planDate DATE;
    
    DECLARE done INT DEFAULT 0;
    DECLARE crs CURSOR FOR SELECT plan_date, plan_day_value FROM plans WHERE plan_date BETWEEN dateStart AND dateEnd AND subdivision_id = subdivisionId;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
    
    SET dateEnd = LAST_DAY(dateStart);
	OPEN crs;
    WHILE NOT done DO
    	FETCH crs INTO planDate, planDayValue;
        IF NOT done THEN 
            SET summ = summ + planDayValue;
            UPDATE plans SET plan_month_value = summ WHERE plan_date = planDate AND subdivision_id = subdivisionId;
        END IF;
    END WHILE;
    CLOSE crs;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `boxes`
--

CREATE TABLE `boxes` (
  `id` int(11) UNSIGNED NOT NULL,
  `box_name` varchar(255) NOT NULL,
  `is_actual` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Структура таблицы `plans`
--

CREATE TABLE `plans` (
  `id` int(10) UNSIGNED NOT NULL,
  `plan_date` date NOT NULL,
  `plan_day_value` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `plan_month_value` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subdivision_id` int(10) UNSIGNED NOT NULL,
  `unit_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Триггеры `plans`
--
DELIMITER $$
CREATE TRIGGER `trg_plans_bi` BEFORE INSERT ON `plans` FOR EACH ROW BEGIN
	SET @unit_date = (SELECT MAX(unit_date) FROM unit_report_subdivisions);
	SET NEW.unit_id = (SELECT unit_id FROM unit_report_subdivisions WHERE unit_date = @unit_date);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `reports`
--

CREATE TABLE `reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `report_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Триггеры `reports`
--
DELIMITER $$
CREATE TRIGGER `trg_report_bi` BEFORE INSERT ON `reports` FOR EACH ROW BEGIN
    SET NEW.user_id = 1;
    SET NEW.created_at = CURRENT_DATE;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trg_reports_ai` AFTER INSERT ON `reports` FOR EACH ROW BEGIN
	DECLARE done INT DEFAULT 0;
    DECLARE subdivision_id INT;
    DECLARE crs CURSOR FOR SELECT id FROM subdivisions WHERE is_actual = 1;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	OPEN crs;
    WHILE NOT done DO
    	FETCH crs INTO subdivision_id;
        IF NOT done THEN 
            INSERT INTO report_subdivisions (report_id,subdivision_id,unit_id)
            VALUES (NEW.id, subdivision_id, 1);
        END IF;
    END WHILE;
    CLOSE crs;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `report_boxes`
--

CREATE TABLE `report_boxes` (
  `id` int(10) UNSIGNED NOT NULL,
  `report_shift_id` int(10) UNSIGNED NOT NULL,
  `box_id` int(10) UNSIGNED NOT NULL,
  `unit_id` int(10) UNSIGNED DEFAULT NULL,
  `box_value` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Триггеры `report_boxes`
--
DELIMITER $$
CREATE TRIGGER `trg_report_boxes_ai` AFTER INSERT ON `report_boxes` FOR EACH ROW BEGIN
	SET @sum_value = (SELECT SUM(box_value) FROM report_boxes WHERE report_shift_id = NEW.report_shift_id);
    UPDATE report_shifts SET shift_value = @sum_value
    WHERE id = NEW.report_shift_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trg_report_boxes_bi` BEFORE INSERT ON `report_boxes` FOR EACH ROW BEGIN
	SET @reportDate = (SELECT report_date FROM
 reports AS r, report_subdivisions AS rsub, report_shifts AS rsh WHERE rsub.report_id = r.id AND rsh.report_subdivision_id = rsub.id  AND rsh.id = NEW.report_shift_id);
 
	SET @unit_date = (SELECT MAX(unit_date) FROM unit_report_boxes WHERE is_actual = 1 AND unit_date <= @reportDate );
    
    SET NEW.unit_id = (SELECT unit_id FROM unit_report_boxes WHERE unit_date = @unit_date);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `report_shifts`
--

CREATE TABLE `report_shifts` (
  `id` int(10) UNSIGNED NOT NULL,
  `report_subdivision_id` int(10) UNSIGNED NOT NULL,
  `shift_id` int(10) UNSIGNED NOT NULL,
  `shift_value` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Триггеры `report_shifts`
--
DELIMITER $$
CREATE TRIGGER `trg_report_shifts_au` AFTER UPDATE ON `report_shifts` FOR EACH ROW BEGIN
	SET @sum_value = (SELECT SUM(shift_value) FROM report_shifts WHERE report_subdivision_id = NEW.report_subdivision_id);
    UPDATE report_subdivisions SET subdivision_value = @sum_value
    WHERE id = NEW.report_subdivision_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trg_report_shifts_bu` BEFORE UPDATE ON `report_shifts` FOR EACH ROW BEGIN
     SET @subdivision_unit_id = (SELECT unit_id FROM report_subdivisions WHERE id = NEW.report_subdivision_id);
 SET @box_unit_id = (SELECT DISTINCT unit_id FROM report_boxes WHERE report_shift_id = NEW.id);
    SET @max_coef_date = (SELECT MAX(unit_convert_date) FROM unit_converts);
    
    SET @coef = (SELECT COALESCE( (SELECT coefficient_value FROM unit_converts WHERE from_unit_id = @box_unit_id AND to_unit_id = @subdivision_unit_id AND unit_convert_date = @max_coef_date AND is_actual = 1), 9) AS coef);
    
	SET NEW.shift_value = @coef*(NEW.shift_value);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `report_subdivisions`
--

CREATE TABLE `report_subdivisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `report_id` int(10) UNSIGNED NOT NULL,
  `subdivision_id` int(10) UNSIGNED NOT NULL,
  `unit_id` int(10) UNSIGNED NOT NULL,
  `subdivision_value` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `plan_day_value` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `deviation_day_value` int(10) NOT NULL DEFAULT '0',
  `subdivision_month_value` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `plan_month_value` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `deviation_month_value` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Триггеры `report_subdivisions`
--
DELIMITER $$
CREATE TRIGGER `trg_report_subdivisions_ai` AFTER INSERT ON `report_subdivisions` FOR EACH ROW BEGIN
	DECLARE done INT DEFAULT 0;
    DECLARE shift_id INT;
    DECLARE crs CURSOR FOR SELECT id FROM shifts WHERE is_actual = 1;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;
	OPEN crs;
    WHILE NOT done DO
    	FETCH crs INTO shift_id;
        IF NOT done THEN 
            INSERT INTO report_shifts (report_subdivision_id,shift_id)
            VALUES (NEW.id, shift_id);
        END IF;
    END WHILE;
    CLOSE crs;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trg_report_subdivisions_bi` BEFORE INSERT ON `report_subdivisions` FOR EACH ROW BEGIN 
    SET @reportDate = (SELECT report_date FROM
 reports AS r WHERE r.id = NEW.report_id);
 
	SET @unit_date = (SELECT MAX(unit_date) FROM unit_report_subdivisions WHERE is_actual = 1 AND unit_date <= @reportDate );
    
    SET NEW.unit_id = (SELECT unit_id FROM unit_report_subdivisions WHERE unit_date = @unit_date);
    
    SET NEW.plan_day_value = (SELECT plan_day_value FROM plans WHERE plan_date = @reportDate AND subdivision_id = NEW.subdivision_id);
    
    SET NEW.plan_month_value = (SELECT plan_month_value FROM plans WHERE plan_date = @reportDate AND subdivision_id = NEW.subdivision_id);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `shifts`
--

CREATE TABLE `shifts` (
  `id` int(10) UNSIGNED NOT NULL,
  `shift_name` varchar(255) NOT NULL,
  `is_actual` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `subdivisions`
--

CREATE TABLE `subdivisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `subdivision_name` varchar(255) NOT NULL,
  `is_actual` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `subdivision_box`
--

CREATE TABLE `subdivision_box` (
  `id` int(10) UNSIGNED NOT NULL,
  `subdivision_id` int(10) UNSIGNED NOT NULL,
  `box_id` int(10) UNSIGNED NOT NULL,
  `is_actual` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `units`
--

CREATE TABLE `units` (
  `id` int(10) UNSIGNED NOT NULL,
  `unit_name` varchar(255) NOT NULL,
  `is_actual` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `unit_converts`
--

CREATE TABLE `unit_converts` (
  `id` int(10) UNSIGNED NOT NULL,
  `unit_convert_date` date NOT NULL,
  `from_unit_id` int(10) UNSIGNED NOT NULL,
  `to_unit_id` int(10) UNSIGNED NOT NULL,
  `coefficient_value` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `is_actual` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Структура таблицы `unit_report_boxes`
--

CREATE TABLE `unit_report_boxes` (
  `id` int(10) UNSIGNED NOT NULL,
  `unit_date` date NOT NULL,
  `unit_id` int(10) UNSIGNED NOT NULL,
  `is_actual` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Структура таблицы `unit_report_subdivisions`
--

CREATE TABLE `unit_report_subdivisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `unit_date` date NOT NULL,
  `unit_id` int(10) UNSIGNED NOT NULL,
  `is_actual` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `is_actual` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `full_name`, `is_actual`) VALUES
(1, 'admin', 'admin', 'admin admin', 1),
(3, 'login', 'password', 'login', 1),
(4, 'user', 'user', 'user', 0);

-- --------------------------------------------------------

--
-- Дублирующая структура для представления `vw_report_shifts`
-- (См. Ниже фактическое представление)
--
CREATE TABLE `vw_report_shifts` (
`id` int(10) unsigned
,`report_subdivision_id` int(10) unsigned
,`shift_id` int(10) unsigned
,`shift_value` int(10) unsigned
,`report_id` int(10) unsigned
,`subdivision_id` int(10) unsigned
,`unit_id` int(10) unsigned
,`subdivision_value` int(10) unsigned
,`report_date` date
,`created_at` timestamp
,`user_id` int(10) unsigned
,`subdivision_name` varchar(255)
,`unit_name` varchar(255)
,`shift_name` varchar(255)
);

-- --------------------------------------------------------

--
-- Структура для представления `vw_report_shifts`
--
DROP TABLE IF EXISTS `vw_report_shifts`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_report_shifts`  AS  select `rsh`.`id` AS `id`,`rsh`.`report_subdivision_id` AS `report_subdivision_id`,`rsh`.`shift_id` AS `shift_id`,`rsh`.`shift_value` AS `shift_value`,`rsd`.`report_id` AS `report_id`,`rsd`.`subdivision_id` AS `subdivision_id`,`rsd`.`unit_id` AS `unit_id`,`rsd`.`subdivision_value` AS `subdivision_value`,`r`.`report_date` AS `report_date`,`r`.`created_at` AS `created_at`,`r`.`user_id` AS `user_id`,`sd`.`subdivision_name` AS `subdivision_name`,`un`.`unit_name` AS `unit_name`,`sh`.`shift_name` AS `shift_name` from (((((`report_shifts` `rsh` join `report_subdivisions` `rsd` on((`rsh`.`report_subdivision_id` = `rsd`.`id`))) join `reports` `r` on((`rsd`.`report_id` = `r`.`id`))) join `subdivisions` `sd` on((`rsd`.`subdivision_id` = `sd`.`id`))) join `units` `un` on((`rsd`.`unit_id` = `un`.`id`))) join `shifts` `sh` on((`rsh`.`shift_id` = `sh`.`id`))) ;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `boxes`
--
ALTER TABLE `boxes`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unit_id` (`unit_id`),
  ADD KEY `subdivision_id` (`subdivision_id`);

--
-- Индексы таблицы `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `report_boxes`
--
ALTER TABLE `report_boxes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `box_id` (`box_id`),
  ADD KEY `unit_id` (`unit_id`),
  ADD KEY `report_shift_id` (`report_shift_id`);

--
-- Индексы таблицы `report_shifts`
--
ALTER TABLE `report_shifts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `report_subdivision_id` (`report_subdivision_id`) USING BTREE;

--
-- Индексы таблицы `report_subdivisions`
--
ALTER TABLE `report_subdivisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `report_id` (`report_id`),
  ADD KEY `subdivision_id` (`subdivision_id`),
  ADD KEY `unit_id` (`unit_id`);

--
-- Индексы таблицы `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `subdivisions`
--
ALTER TABLE `subdivisions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `subdivision_box`
--
ALTER TABLE `subdivision_box`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subdivision_id` (`subdivision_id`),
  ADD KEY `box_id` (`box_id`);

--
-- Индексы таблицы `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `unit_converts`
--
ALTER TABLE `unit_converts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_unit_id` (`from_unit_id`),
  ADD KEY `to_unit_id` (`to_unit_id`);

--
-- Индексы таблицы `unit_report_boxes`
--
ALTER TABLE `unit_report_boxes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unit_id` (`unit_id`);

--
-- Индексы таблицы `unit_report_subdivisions`
--
ALTER TABLE `unit_report_subdivisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unit_id` (`unit_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `boxes`
--
ALTER TABLE `boxes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `report_boxes`
--
ALTER TABLE `report_boxes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `report_shifts`
--
ALTER TABLE `report_shifts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `report_subdivisions`
--
ALTER TABLE `report_subdivisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `shifts`
--
ALTER TABLE `shifts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `subdivisions`
--
ALTER TABLE `subdivisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `subdivision_box`
--
ALTER TABLE `subdivision_box`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `units`
--
ALTER TABLE `units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `unit_converts`
--
ALTER TABLE `unit_converts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `unit_report_boxes`
--
ALTER TABLE `unit_report_boxes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `unit_report_subdivisions`
--
ALTER TABLE `unit_report_subdivisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `plans`
--
ALTER TABLE `plans`
  ADD CONSTRAINT `plans_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `plans_ibfk_2` FOREIGN KEY (`subdivision_id`) REFERENCES `subdivisions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `reports`
--
ALTER TABLE `reports`
  ADD CONSTRAINT `FK2o32rer9hfweeylg7x8ut8rj2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `report_boxes`
--
ALTER TABLE `report_boxes`
  ADD CONSTRAINT `FK2a0h1jc57n1aib5b982422r89` FOREIGN KEY (`box_id`) REFERENCES `boxes` (`id`),
  ADD CONSTRAINT `FK4e3gg45eo0oo2gby9o7gs1by5` FOREIGN KEY (`report_shift_id`) REFERENCES `report_shifts` (`id`),
  ADD CONSTRAINT `FKqygt50yu7co3qgy3qlc29mgfa` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`),
  ADD CONSTRAINT `report_boxes_ibfk_2` FOREIGN KEY (`box_id`) REFERENCES `boxes` (`id`),
  ADD CONSTRAINT `report_boxes_ibfk_3` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`),
  ADD CONSTRAINT `report_boxes_ibfk_4` FOREIGN KEY (`report_shift_id`) REFERENCES `report_shifts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `report_shifts`
--
ALTER TABLE `report_shifts`
  ADD CONSTRAINT `report_shifts_ibfk_1` FOREIGN KEY (`report_subdivision_id`) REFERENCES `report_subdivisions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `report_shifts_ibfk_2` FOREIGN KEY (`shift_id`) REFERENCES `shifts` (`id`);

--
-- Ограничения внешнего ключа таблицы `report_subdivisions`
--
ALTER TABLE `report_subdivisions`
  ADD CONSTRAINT `report_subdivisions_ibfk_1` FOREIGN KEY (`report_id`) REFERENCES `reports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `report_subdivisions_ibfk_2` FOREIGN KEY (`subdivision_id`) REFERENCES `subdivisions` (`id`),
  ADD CONSTRAINT `report_subdivisions_ibfk_3` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`);

--
-- Ограничения внешнего ключа таблицы `subdivision_box`
--
ALTER TABLE `subdivision_box`
  ADD CONSTRAINT `subdivision_box_ibfk_1` FOREIGN KEY (`subdivision_id`) REFERENCES `subdivisions` (`id`),
  ADD CONSTRAINT `subdivision_box_ibfk_2` FOREIGN KEY (`box_id`) REFERENCES `boxes` (`id`);

--
-- Ограничения внешнего ключа таблицы `unit_converts`
--
ALTER TABLE `unit_converts`
  ADD CONSTRAINT `unit_converts_ibfk_1` FOREIGN KEY (`from_unit_id`) REFERENCES `units` (`id`),
  ADD CONSTRAINT `unit_converts_ibfk_2` FOREIGN KEY (`to_unit_id`) REFERENCES `units` (`id`);

--
-- Ограничения внешнего ключа таблицы `unit_report_boxes`
--
ALTER TABLE `unit_report_boxes`
  ADD CONSTRAINT `unit_report_boxes_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`);

--
-- Ограничения внешнего ключа таблицы `unit_report_subdivisions`
--
ALTER TABLE `unit_report_subdivisions`
  ADD CONSTRAINT `unit_report_subdivisions_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
